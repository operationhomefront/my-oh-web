﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using Kendo.Mvc.Extensions;
using OH.DAL;
using OH.Net.Models;

namespace OH.Net.Code.Authorization.Repositories
{
    public class MyOhRoleRepository : Repository<MyOHRole>, IMyOhRoleRepository
    {
        public MyOhRoleRepository(ApplicationDbContext myOhContext)
            : base(myOhContext)
        {          
        }
    }
}