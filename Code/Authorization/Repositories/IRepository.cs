﻿using System;
using System.Collections.Generic;

namespace OH.Net.Code.Authorization.Repositories
{
    public interface IRepository<T> : IDisposable where T : class
    {
        T GetById(Guid id);
        IEnumerable<T> GetAll();
        //IEnumerable<object> GetAllEntitiesByGuid(object guid);
        Guid AddOrUpdate(T entity);
        void Delete(Guid id);
        void Save();
        
        Guid GetLastInsertedId();

    }
}
