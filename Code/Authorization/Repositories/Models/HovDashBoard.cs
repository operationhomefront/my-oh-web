﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.DynamicData;
using Microsoft.Data.OData.Query.SemanticAst;

namespace OH.Net.Code.Authorization.Repositories.Models
{
    public class HovDashBoard
    {
        
       public ICollection<MyOHUser> _ohUsers { get; set; }


        public int GetAdminRoles()
        {
            return _ohUsers.Count(u => (u.Roles.All(r => r.Name == "HOV:Administrator")));
        }

        public int GetCareGiverRoles()
        {
            return _ohUsers.Count(u => (u.Roles.All(r => r.Name == "HOV:Caregiver")));
        }

        public int GetSupportLeaderRoles()
        {
            return _ohUsers.Count(u => (u.Roles.All(r => r.Name == "HOV:Support Group Leader")));
        }

        public int GetInquiryRoles()
        {
            return _ohUsers.Count(u => (u.Roles.All(r => r.Name == "HOV:Inquiry")));
        }
    }
}