﻿using System.Collections.Generic;
using OH.DAL.Domain;

namespace OH.Net.Code.Authorization.Repositories
{
    public interface IMyOhRoleRepository : IRepository<MyOHRole>
    {
        
    }
}
