﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OH.Net.Code.Authorization.Repositories.Models;
namespace OH.Net.Code.Authorization.Repositories.ReportBlls
{
    public interface IReportBllServiceUi
    {
        IEnumerable<MyOHRole> GetAllRoles();

        IEnumerable<MyOHUser> GetAllUsers();

        IEnumerable<MyOHUser> GetAllUsersByRole(string role);

        HovDashBoard GetHovDashBoard();
    }
}
