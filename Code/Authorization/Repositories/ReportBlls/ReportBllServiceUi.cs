﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OH.DAL.Repositories.Meetings;
using OH.Net.Code.Authorization.Repositories.Models;


namespace OH.Net.Code.Authorization.Repositories.ReportBlls
{
    public class  ReportBllServiceUi : IReportBllServiceUi
    {
        public readonly IMyOhRoleRepository _MyOhRoleRepository;
        public readonly IMyOhUserRepository _MyOhUserRepository;
        public readonly IMeetingRepository _MyOhMeetingRepository; 
        public ReportBllServiceUi(IMyOhRoleRepository myOhRoleRepository, IMyOhUserRepository myOhUserRepository, IMeetingRepository myOhMeetingRepository)
        {
            this._MyOhRoleRepository = myOhRoleRepository;
            this._MyOhUserRepository = myOhUserRepository;
            this._MyOhMeetingRepository = myOhMeetingRepository;
        }

        public IEnumerable<MyOHRole> GetAllRoles()
        {
            return _MyOhRoleRepository.GetAll();
        }

        public IEnumerable<MyOHUser> GetAllUsers()
        {
            return _MyOhUserRepository.GetAll();
        }

        public IEnumerable<MyOHUser> GetAllUsersByRole(string role)
        {
            return from u in _MyOhUserRepository.GetAll()
                   where u.Roles.All(a => a.Name == "HOV:Administrator")
                select u;

        }

        public HovDashBoard GetHovDashBoard()
        {
            HovDashBoard hd = new HovDashBoard();
            hd._ohUsers = _MyOhUserRepository.GetAll().ToList();
            return hd;
        }
    }
}
