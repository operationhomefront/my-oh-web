﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using OH.Net.Models;

namespace OH.Net.Code.Authorization.Repositories
{
    public class MyOhUserRepository : Repository<MyOHUser>, IMyOhUserRepository
    {
        public MyOhUserRepository(ApplicationDbContext myOhContext)
            : base(myOhContext)
        {

        }
    }
}