﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.EnterpriseServices.Internal;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Antlr.Runtime.Misc;
using OH.BLL.Models.Attachments;
using OH.BLL.Models.Persons;
using OH.BLL.Models.ProgramProfiles;
using OH.BLL.Models.ProgramProfiles.Caregiver;
using OH.BLL.Models.ProgramProfiles.Military;
//using OHv4.Models.Donations;

namespace OH.Net.Code.viewModels 
{
    public class vmInquiryWorkFlowBase
    {
        public ProfileMilitaryViewModel MilitaryProfile { get; set; }
        public ProfileCaregiverViewModel Submitter { get; set; }        
        public Collection<vmWorkFlowDisplay> Displays { get; set; } 

        public vmInquiryWorkFlowBase()
        {
            Displays = new Collection<vmWorkFlowDisplay>();
            //RouteValueDictionary r = new RouteValueDictionary(new
            //    {
            //        parameterOne = "Hello",
            //        parameterTwo = "World"
            //    });


            Displays.Add(new vmWorkFlowDisplay
            {
                CategoryId = new Guid("71BA2DA8-746B-42B9-94D6-A39AA8ECF7DB"),
                Action = "InitialCareGiver",
                Controller = "InitialInquiry",
                SectionName = "Caregiver",
                //Values = r,
                Enabled = true,
                StepNumber = 1,
                Selected = true
            });
            Displays.Add(new vmWorkFlowDisplay { Action = "ServiceMember", Controller = "InitialInquiry", SectionName = "Service Member", StepNumber = 2 });
            Displays.Add(new vmWorkFlowDisplay { Action = "Summary", Controller = "InitialInquiry", SectionName = "Final Step", StepNumber = 3 });

           
            //MilitaryProfile = new MilitaryProfileViewModel();
            //MilitaryProfile.Attachments = new Collection<UploadAttachmentViewModel>();
        }

    }
    
    public class vmWorkFlowDisplay
    {
        public vmWorkFlowDisplay()
        {
            Enabled = false;
            Selected = false;
        }
        public string SectionName { get; set; }
        public string Action { get; set; }
        public string Controller { get; set; }
        public bool Enabled { get; set; }
        public int StepNumber { get; set; }
        public bool Selected { get; set; }

        public Guid CategoryId { get; set; }
        public Guid OwnerId { get; set; }
        public RouteValueDictionary Values { get; set; }
    }

        
}