﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.EnterpriseServices.Internal;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//using Microsoft.Data.OData.Query.SemanticAst;
using Microsoft.Owin.Security.Provider;
using OH.Net.Models;

namespace OH.Net.Code.viewModels
{

    public class vmWorkflowAccountCreate
    {
        public vmWorkflowAccountCreate()
        {
            //Possibly will be used later 
            this.ProfileSelector1 = new List<GenericList>();

            ProfileSelector1.Add(new GenericList() { InternalName = "Service Member/Veteran or Spouse", InternalValue = 1 });
            ProfileSelector1.Add(new GenericList() { InternalName = "PersonCaregiver of Service Member/Veteran", InternalValue = 2 });
            ProfileSelector1.Add(new GenericList() { InternalName = "Volunteer", InternalValue = 4 });
            
            this.ProfileSelector2 = new List<GenericList>();

            ProfileSelector2.Add(new GenericList() { InternalName = "Apply for emergency financial assistance.", InternalValue = 64 });
            ProfileSelector2.Add(new GenericList() { InternalName = "View and/or register for events in my area.", InternalValue = 128 });
            ProfileSelector2.Add(new GenericList() { InternalName = "Apply for a mortgage-free home.", InternalValue = 256 });
            ProfileSelector2.Add(new GenericList() { InternalName = "Join Operation Homefront’s caregiver program.", InternalValue = 512 });
            ProfileSelector2.Add(new GenericList() { InternalName = "Apply for free transitional housing.", InternalValue = 1024 });
            ProfileSelector2.Add(new GenericList() { InternalName = "Sign up to pick up donations from Dollar Tree stores.", InternalValue = 2048 });

            vmRegister = new RegisterViewModel();
            ErrorCount = 0;
            ErrorMessage = "";
        }
        //Possibly will be used later 
        //public ICollection<GenericList> ProfileSelector1List { get; set; }
        //public ICollection<GenericList> ProfileSelector2List { get; set; }

        
        public List<GenericList> ProfileSelector1 { get; set; }

       
        public List<GenericList> ProfileSelector2 { get; set; }

        [Required(ErrorMessage = "Select")]
        public List<string> ProfileSelected1 { get; set; }

        [Required(ErrorMessage="Select")]
        public List<string> ProfileSelected2 { get; set; }

        [NotMapped]
        public int ErrorCount { get; set; }
        [NotMapped]
        public string ErrorMessage { get; set; }

        [NotMapped]
        [PlaceHolder("ConfirmationCode")]
        public string ConfirmationCode { get; set; }

        public RegisterViewModel vmRegister { get; set; }
   }


    public class GenericList
    {
        public string InternalName { get; set; }
        public int InternalValue { get; set; }

        public GenericList()
        {
            //TODO: Complete member initialization
          
        }
    }

}