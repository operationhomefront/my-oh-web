﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Security;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
//using Microsoft.Data.OData.Query.SemanticAst;
using OH.DAL;
using OH.DAL.Domain.Applications;
using OH.DAL.Domain.Persons;
using OH.Net.Models;


namespace OH.Net.Code
{


    static class Conversion<TInput, TOutput>
    {
        private static readonly Func<TInput, TOutput> Converter;

        static Conversion()
        {
            Converter = CreateConverter();
        }

        public static Func<TInput, TOutput> CreateConverter()
        {
            var input = Expression.Parameter(typeof(TInput), "input");

            // For each property that exists in the destination object, is there a property with the same name in the source object?
            var destinationProperties = typeof(TOutput)
                .GetProperties(BindingFlags.Public | BindingFlags.Instance)
                .Where(prop => prop.CanWrite);
            var sourceProperties = typeof(TInput)
                .GetProperties(BindingFlags.Public | BindingFlags.Instance)
                .Where(prop => prop.CanRead);

            var memberBindings = sourceProperties.Join(destinationProperties,
                sourceProperty => sourceProperty.Name,
                destinationProperty => destinationProperty.Name,
                (sourceProperty, destinationProperty) =>
                    (MemberBinding)Expression.Bind(destinationProperty,
                        Expression.Property(input, sourceProperty)));

            var body = Expression.MemberInit(Expression.New(typeof(TOutput)), memberBindings);
            var lambda = Expression.Lambda<Func<TInput, TOutput>>(body, input);
            return lambda.Compile();
        }

        public static TOutput From(TInput input)
        {
            return Converter(input);
        }
    }

    public class MyOHSecurityGroup
    {
        public MyOHSecurityGroup()
        {
            Id = Guid.NewGuid();
        }

        public static MyOHSecurityGroup GetGroup(Guid id,string Name,Guid application)
        {
            return new MyOHSecurityGroup { Id = id,Name=Name,ApplicationId = application };
        }



        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ClusterId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; set; }

        public string Name { get; set; }
        public Guid ApplicationId { get; set; }

        //public virtual Application Application { get; set; }
    }

    public class MyOHUserMembership
    {
        public MyOHUserMembership()
        {
            Id = Guid.NewGuid();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ClusterId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; private set; }

        public virtual MyOHUser User { get; set; }
        public DateTime CreatedDateTime { get; set; }
        public string ConfirmationToken { get; set; }
        public bool Confirmed { get; set; }
        public DateTime? LastLoginFailed { get; set; }
        public int LoginFailureCounter { get; set; }
        public string PasswordHash { get; set; }
        public string PasswordSalt { get; set; }

        public DateTime? PasswordChanged { get; set; }

        public string VerificationToken { get; set; }
        public DateTime? VerificationTokenExpires { get; set; }
    }

    public class MyOHRole : IRole<Guid>
    {
        public MyOHRole()
        {
            Id = Guid.NewGuid();
        }
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ClusterId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; set; }

        public string Name { get; set; }

        public virtual MyOHSecurityGroup Group { get; set; }

        public virtual ICollection<MyOHUser> Users { get; set; }

        Guid IRole<Guid>.Id
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        string IRole<Guid>.Name
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }
    }

    public class MyOHOAuthMembership
    {
        public MyOHOAuthMembership()
        {
            Id = Guid.NewGuid();
        }
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ClusterId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; private set; }

        public virtual MyOHUser User { get; set; }
        public string LoginProvider { get; set; }
        public string ProviderKey { get; set; }
    }

    public class MyOHUser :  IUser<Guid>
    {

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ClusterId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; set; }

        public string UserName { get; set; }

        public virtual string Email { get; set; }

        public MyOHUserMembership PasswordLogin { get; set; }
        public virtual ICollection<MyOHOAuthMembership> ExternalLogins { get; set; }

        public virtual ICollection<MyOHRole> Roles { get; set; }

        public Guid ApplicationId { get; set; }
        public string MobileAlias { get; set; }
        public bool IsAnonymous { get; set; }
        public System.DateTime LastActivityDate { get; set; }
        public string MobilePIN { get; set; }
        public string LoweredEmail { get; set; }
        public string LoweredUserName { get; set; }
        public string PasswordQuestion { get; set; }
        public string PasswordAnswer { get; set; }
        public bool IsApproved { get; set; }
        public bool IsLockedOut { get; set; }
        public System.DateTime CreateDate { get; set; }
        public System.DateTime LastLoginDate { get; set; }
        public System.DateTime LastPasswordChangedDate { get; set; }
        public System.DateTime LastLockoutDate { get; set; }
        public int FailedPasswordAttemptCount { get; set; }
        public System.DateTime FailedPasswordAttemptWindowStart { get; set; }
        public int FailedPasswordAnswerAttemptCount { get; set; }
        public System.DateTime FailedPasswordAnswerAttemptWindowStart { get; set; }
        public string Comment { get; set; }


        public MyOHUser()
        {
            Id = Guid.NewGuid();
            CreateDate = DateTime.Now;
            IsApproved = false;
            LastLoginDate = DateTime.Now;
            LastActivityDate = DateTime.Now;
            LastPasswordChangedDate = DateTime.Now;
            LastLockoutDate = DateTime.Parse("1/1/1754");
            FailedPasswordAnswerAttemptWindowStart = DateTime.Parse("1/1/1754");
            FailedPasswordAttemptWindowStart = DateTime.Parse("1/1/1754");
            ExternalLogins = new Collection<MyOHOAuthMembership>();
            Roles = new Collection<MyOHRole>();
        }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<MyOHUser,Guid> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here

            return userIdentity;
        }

        
        string IUser<Guid>.UserName
        {
            get { return UserName; }
            set { UserName = value; }
        }

        Guid IUser<Guid>.Id
        {
            get { return Id; }
        }
    }

    public class MyOHMembershipStore : IUserLoginStore<MyOHUser, Guid>,
        IUserRoleStore<MyOHUser, Guid>, IUserPasswordStore<MyOHUser, Guid>, IUserEmailStore<MyOHUser, Guid>, IUserLockoutStore<MyOHUser, Guid>, IQueryableUserStore<MyOHUser,Guid>
    {
        private readonly ApplicationDbContext _context;

        public MyOHMembershipStore(ApplicationDbContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException("context");

            }
            _context = context;
        }

        public async Task CreateAsync(MyOHUser user)
        {
            MyOHUserMembership passwordLogin = user.PasswordLogin;

            if (passwordLogin != null)
            {
                passwordLogin.CreatedDateTime = DateTime.Now;
                passwordLogin.Confirmed = true;

                if (passwordLogin.PasswordHash == null)
                {
                    passwordLogin.PasswordHash = string.Empty;
                }

                passwordLogin.PasswordSalt = string.Empty;

            }
            _context.MyOHUsers.Add(user);
            await _context.SaveChangesAsync();

        }

        public Task DeleteAsync(MyOHUser user)
        {
            throw new NotSupportedException();
        }

        public Task<MyOHUser> FindByIdAsync(Guid userId)
        {
            return _context.MyOHUsers.FirstOrDefaultAsync(user => user.Id == userId);
        }

        public Task<MyOHUser> FindByNameAsync(string username)
        {
            return _context.MyOHUsers.Include(t=>t.PasswordLogin).FirstOrDefaultAsync(account => account.UserName == username);
        }

        public Task UpdateAsync(MyOHUser user)
        {
            return _context.SaveChangesAsync();
        }

        public void Dispose()
        {
            
        }

        public Task AddLoginAsync(MyOHUser user, UserLoginInfo login)
        {
            if (user == null)
            {
                throw new ArgumentNullException();
            }
            if (user.ExternalLogins == null)
            {
                throw new ArgumentNullException();
            }
            if (login == null)
            {
                throw new ArgumentNullException();
            }
            user.ExternalLogins.Add(new MyOHOAuthMembership
            {
                LoginProvider = login.LoginProvider,
                ProviderKey = login.ProviderKey
            });
            return Task.FromResult<object>(null);
        }

        public Task<MyOHUser> FindAsync(UserLoginInfo login)
        {
            if (login == null)
            {
                return Task.FromResult<MyOHUser>(null);

            }

            return _context.MyOHUsers.SingleOrDefaultAsync(
                    u =>
                        u.ExternalLogins.Any(
                            l => l.LoginProvider == login.LoginProvider && l.ProviderKey == login.ProviderKey));

        }

        public Task<IList<UserLoginInfo>> GetLoginsAsync(MyOHUser user)
        {
            if (user == null)
            {
                throw new ArgumentNullException();
            }
            if (user.ExternalLogins == null)
            {
                throw new ArgumentNullException();
            }

            IList<UserLoginInfo > logins = new List<UserLoginInfo>();

            foreach (MyOHOAuthMembership login in user.ExternalLogins)
            {
                       logins.Add(new UserLoginInfo(login.LoginProvider,login.ProviderKey));
            }
            return Task.FromResult(logins);
        }

        public Task RemoveLoginAsync(MyOHUser user, UserLoginInfo login)
        {
            if (user == null)
            {
                throw new ArgumentNullException();
            }
            if (user.ExternalLogins == null)
            {
                throw new ArgumentNullException();
            }
            if (login == null)
            {
                throw new ArgumentNullException();
            }

            MyOHOAuthMembership toRemove =
                user.ExternalLogins.Single(
                    l => l.LoginProvider == login.LoginProvider && l.ProviderKey == login.ProviderKey);
            user.ExternalLogins.Remove(toRemove);
            _context.Entry(toRemove).State = EntityState.Deleted;

            return Task.FromResult<object>(null);

        }

        public async Task AddToRoleAsync(MyOHUser user, string role)
        {
            //"HOV:Iquiry:Sub1"

            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            if (user.Roles == null)
            {
                throw new ArgumentNullException("user");
            }
            if (role == null)
            {
                throw new ArgumentNullException("role");
            }

            var toAdd = await _context.MyOHRoles.SingleAsync(r => r.Name == role);
            user.Roles.Add(toAdd);

        }

        public Task<IList<string>> GetRolesAsync(MyOHUser user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            if (user.Roles == null)
            {
                throw new ArgumentNullException("user");
            }

            IList<string> roles = new List<string>();

            foreach (MyOHRole role in user.Roles)
            {
                roles.Add(role.Name);
            }
            return Task.FromResult(roles);
        }

        public Task<bool> IsInRoleAsync(MyOHUser user, string role)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            if (user.Roles == null)
            {
                throw new ArgumentNullException("user");
            }
            if (role == null)
            {
                throw new ArgumentNullException("role");
            }
            bool inRole = user.Roles.Any(r => r.Name == role);
            return Task.FromResult(inRole);

        }

       

        public Task RemoveFromRoleAsync(MyOHUser user, string role)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            if (user.Roles == null)
            {
                throw new ArgumentNullException("user");
            }
            if (role == null)
            {
                throw new ArgumentNullException("role");
            }
            var toRemove = user.Roles.Single(r => r.Name == role);
            user.Roles.Remove(toRemove);

            return Task.FromResult<object>(null);
        }

        public Task<string> GetPasswordHashAsync(MyOHUser user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            if (user.PasswordLogin == null)
            {
                throw new ArgumentNullException("user"); 
            }
            return Task.FromResult(user.PasswordLogin.PasswordHash);
        }

        public Task<bool> HasPasswordAsync(MyOHUser user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            return Task.FromResult(user.PasswordLogin != null);
        }

        public Task SetPasswordHashAsync(MyOHUser user, string passwordHash)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            if (user.PasswordLogin == null)
            {
                user.PasswordLogin = new MyOHUserMembership();
                user.PasswordLogin.CreatedDateTime = DateTime.Now;
                user.PasswordLogin.PasswordSalt = "";

            }
            user.PasswordLogin.PasswordHash = passwordHash;
            return Task.FromResult<object>(null);
        }

        public class MyOHMembershipRoleStore : IRoleStore<MyOHRole, Guid>, IQueryableRoleStore<MyOHRole,Guid>
        {
            private readonly ApplicationDbContext _context;

            public MyOHMembershipRoleStore(ApplicationDbContext context)
            {
                if (context == null)
                {
                    throw new ArgumentNullException("context");
                }
                _context = context;

            }

            public Task CreateAsync(MyOHRole role)
            {
                _context.MyOHRoles.Add(role);
                return _context.SaveChangesAsync();
            }

            public Task DeleteAsync(MyOHRole role)
            {
                throw new NotImplementedException();
            }

            public Task<MyOHRole> FindByIdAsync(Guid roleId)
            {
                var val = _context.MyOHRoles.SingleOrDefaultAsync(r => r.Id == roleId);

                return Task.FromResult<MyOHRole>(val.Result as MyOHRole);
            }

            public Task<MyOHRole> FindByNameAsync(string roleName)
            {
                var val = _context.MyOHRoles.SingleOrDefaultAsync(r => r.Name == roleName);

                return Task.FromResult<MyOHRole>(val.Result as MyOHRole);
            }

            public Task UpdateAsync(MyOHRole role)
            {
                return _context.SaveChangesAsync();
            }

           

            public void Dispose()
            {
            }

            public IQueryable<MyOHRole> Roles
            {
                get
                {
                    var val = _context.MyOHRoles;
                    return val as IQueryable<MyOHRole>;
                }
            }
        }


        public Task<MyOHUser> FindByEmailAsync(string email)
        {
            var val = _context.MyOHUsers.FirstOrDefaultAsync(u=>u.Email == email);
            return Task.FromResult<MyOHUser>(val.Result as MyOHUser);
        }

        public Task<string> GetEmailAsync(MyOHUser user)
        {
            return Task.FromResult(user.Email);
        }

        public Task<bool> GetEmailConfirmedAsync(MyOHUser user)
        {
            if (user.PasswordLogin == null)
            {
                throw new ArgumentNullException("user");
            }
            return Task.FromResult(user.PasswordLogin.Confirmed);
        }

        public Task SetEmailAsync(MyOHUser user, string email)
        {
            throw new NotImplementedException();
        }

        public Task SetEmailConfirmedAsync(MyOHUser user, bool confirmed)
        {
            throw new NotImplementedException();
        }

        public Task<int> GetAccessFailedCountAsync(MyOHUser user)
        {
            throw new NotImplementedException();
        }

        public Task<bool> GetLockoutEnabledAsync(MyOHUser user)
        {
            return Task.FromResult(false);
        }

        public Task<DateTimeOffset> GetLockoutEndDateAsync(MyOHUser user)
        {
            
            return Task.FromResult(DateTimeOffset.UtcNow.AddDays(1000));
        }

        public Task<int> IncrementAccessFailedCountAsync(MyOHUser user)
        {
            throw new NotImplementedException();
        }

        public Task ResetAccessFailedCountAsync(MyOHUser user)
        {
            user.PasswordLogin.LoginFailureCounter = 0;
            _context.SaveChanges();

            return Task.FromResult<object>(null);
        }

        public Task SetLockoutEnabledAsync(MyOHUser user, bool enabled)
        {
            throw new NotImplementedException();
        }

        public Task SetLockoutEndDateAsync(MyOHUser user, DateTimeOffset lockoutEnd)
        {
            throw new NotImplementedException();
        }

        public IQueryable<MyOHUser> Users
        {
            get
            {
                return _context.MyOHUsers as IQueryable<MyOHUser>;
                //;.Select(u => new MyOHUser { Id = u.Id, UserName = u.UserName }).AsQueryable();
            }
        }
    }


}
