﻿$(function () {
    $('img.color-on-hover').each(function () {
        //
        // The grayscale image will load with the <img tag
        //
        // Preload the color version of the image before we need it.
        //
        var colorImage = new Image();
        colorImage.src = $(this).attr('data-color');
        
        $(this).hover(
            //
            // Enter
            function(event) {
                var image = $(this).attr('data-color');
                $(this).attr('src', image);
            },
            //
            // Leave
            function(event) {
                var image = $(this).attr('data-grayscale');
                $(this).attr('src', image);
            }
        );
    });
});