﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using OH.BLL.Models.Reports;
using OH.BLL.Services.Reports;
using OH.Net.Areas.ReportsArea.Models;


namespace OH.Net.Areas.ReportsArea.Controllers
{
    public class ReportsHomeController : Controller
    {
        public readonly IReportsBllService _reportsBllService;

        public ReportsHomeController(IReportsBllService reportsBllService)
        {
            _reportsBllService = reportsBllService;
        }


        // GET: Reports/Home
        public ActionResult Index()
        {
            _reportsBllService.GetAllActiveCollectionEvents();
            return View();
        }

        public ActionResult IndexOld()
        {
            return View();
        }


        public ActionResult ReportViewer()
        {
            return View();
        }

        public ActionResult HovDashBoard()
        {
            HovDashBoard 

            hd = new HovDashBoard();
            return View(hd);
        }

        public ActionResult RallyPointDashBoard()
        {
            RallyPointDash rpd = new RallyPointDash();

            rpd = _reportsBllService.BuildRallyPointDash(rpd);
             
            return View(rpd);
        }

        public ActionResult DistributionRegistrationTable(Guid eventId)
        {
            _reportsBllService.DistributionRegistrationTable(eventId);

           return null;
            
        }

        public ActionResult CustomReportViewer()
        {
            return View();
        }
        [HttpPost]
        public ActionResult ApplicationDropDownChange(Guid id)
        {
            return Json(_reportsBllService.GetAllReportsByAppId(id), JsonRequestBehavior.AllowGet);
        }

        public ActionResult ReportFilterPartial()
        {
            var reportFilter = new ReportFilter();
            reportFilter.ListReportsAvalable = _reportsBllService.GetAllReports();
            reportFilter.ApplicationList = _reportsBllService.GetAllApplications();
            return PartialView("_ReportFilterPartialPage", reportFilter);
        }

        public ActionResult ReportGridPartial()
        {
            
            return PartialView("_ReportGridPartialPage");
        }

        public ActionResult ExcelExportSave(string contentType, string base64, string fileName)
        {
            var fileContents = Convert.FromBase64String(base64);

            return File(fileContents,contentType,fileName);
        }

        public ActionResult GridRead([DataSourceRequest] DataSourceRequest request, DateTime bgnDateTime, DateTime endDateTime, bool includeArmy, bool includeNavy, bool includeMarines, bool includeAirForce)
        {

            DataTable table3 = _reportsBllService.GetMafaReport(bgnDateTime, endDateTime, includeArmy, includeNavy,
                includeMarines, includeAirForce);

            return Json(table3.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

    
        [HttpPost]
        public ActionResult ReloadReport([DataSourceRequest] DataSourceRequest request, DateTime bgnDateTime, DateTime endDateTime, bool includeArmy, bool includeNavy, bool includeMarines, bool includeAirForce, string includeReport)
        {

            DataTable table3 = _reportsBllService.BuildReportFromStoredProcedure(bgnDateTime, endDateTime, includeArmy, includeNavy,
                includeMarines, includeAirForce, includeReport);

            return Json(table3.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
    }
}
