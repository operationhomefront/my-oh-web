﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OH.Net.Areas.ReportsArea.Models
{
    public class BarChart : Chart
    {
        public ICollection<BarChartSeries> BarChartData { get; set; }

        public string[] Catagories { get; set; }
        public bool VisibleGridLines { get; set; }
        public BarChart()
        {
            this.BarChartData = new List<BarChartSeries>();
            this.VisibleGridLines = true;
        }
    }

    public class BarChartSeries
    {
        public string SeriesName { get; set; }
        public double[] BarDataValue { get; set; }
    }
}