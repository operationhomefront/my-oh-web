﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OH.Net.Areas.ReportsArea.Models
{
    public class LineChart : Chart
    {
        public ICollection<LineChartSeries> LineChartData { get; set; }

        public LineChart()
        {
            this.LineChartData = new List<LineChartSeries>();
        }
    }

    public class LineChartSeries
    {
        public string SeriesName { get; set; }
        public double[] LineDataValue { get; set; }

}
}