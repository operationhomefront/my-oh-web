﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OH.Net.Areas.ReportsArea.Models
{
    public class Chart
    {
        public string ChartName { get; set; }
        public string ChartTitle { get; set; }
        public bool ShowToolTips { get; set; }
        public bool ShowLegend { get; set; }
        public bool ShowLabels { get; set; }

        public Chart()
        {
            this.ShowToolTips = true;
            this.ShowLegend = true;
            this.ShowLabels = true;
        }
    }
}