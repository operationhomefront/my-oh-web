﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.Ajax.Utilities;
using OH.BLL.Models.Reports;
using OH.BLL.Models.Applications;

namespace OH.Net.Areas.ReportsArea.Models
{
    public class ReportFilter
    {
        public DateTime BeginDateTime { get; set; }
        public DateTime EndDateTime { get; set; }

        public bool IncludeArmy { get; set; }
        public bool IncludeNavy { get; set; }
        public bool IncludeAirForce { get; set; }
        public bool IncludeMarines { get; set; }
        public List<string> ReportsAvailable { get; set; }
        public IEnumerable<ApplicationViewModel> ApplicationList { get; set; }
        public SelectList ListOfReports { get; set; }
        public IEnumerable<ReportViewModel> ListReportsAvalable { get; set; }



        public ReportFilter()
        {
            this.IncludeAirForce = true;
            this.IncludeArmy = true;
            this.IncludeMarines = true;
            this.IncludeNavy = true;
            this.ReportsAvailable = new List<string>();
            this.ReportsAvailable.Add("Mafa Money Dispursed");
            this.ReportsAvailable.Add("Mafa Donations Collected");
            this.ListOfReports = new SelectList(ReportsAvailable);
            this.BeginDateTime = DateTime.Now;
            this.EndDateTime = DateTime.Now;

        }

    }
}