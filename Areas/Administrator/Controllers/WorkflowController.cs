﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OH.BLL.Services.Workflow;

namespace OH.Net.Areas.Administrator.Controllers
{
    public class WorkflowController : Controller
    {
        // GET: Administrator/Workflow
         public IWorkflowBLLService _workflowBLLService { get; set; }

         public WorkflowController(IWorkflowBLLService workflowBLLService)
        {
            _workflowBLLService = workflowBLLService;
        }

         public PartialViewResult Index()
        {
            ViewBag.Tasks = _workflowBLLService.GetWorkflowItemViewModel();
            ViewBag.Dependencies =_workflowBLLService.GetDependencyViewModel();
            return PartialView("Index");
        }

        public PartialViewResult WorkflowItem()
        {

            return PartialView("_editWorkflowItem");
        }
    }
}