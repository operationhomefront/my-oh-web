﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Description;
using System.Web.Mvc;
using System.Web.Routing;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Newtonsoft.Json;
using OH.BLL.Models.Persons;
using OH.BLL.Models.Navigation;
using OH.BLL.Models.Reports;
using OH.BLL.Services.Workflow;
using OH.Net.Code;
using OH.Net.Models;

namespace OH.Net.Areas.Administrator.Controllers
{
    [Authorize]
    public class AdminController : Controller
    {
          private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;
        private ApplicationRoleManager _roleManager;
        public IWorkflowBLLService _workflowBLLService { get; set; }


        public AdminController()
        {
        }

        public AdminController(ApplicationUserManager userManager, ApplicationSignInManager signInManager, ApplicationRoleManager roleManager, IWorkflowBLLService workflowBLLService)
        {
            UserManager = userManager;
            SignInManager = signInManager;
            RoleManager = roleManager;
            _workflowBLLService = workflowBLLService;

        }
        public ApplicationRoleManager RoleManager
        {
            get
            {
                return _roleManager ?? HttpContext.GetOwinContext().Get<ApplicationRoleManager>();
            }
            private set
            {
                _roleManager = value;
            }
        }
        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }


        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        // GET: Administrator/Admin
        public ActionResult Index()
        {
            ViewBag.inline = GetInlineData();
            return View();
        }

        public PartialViewResult UserAccount()
        {

            return PartialView();
        }

        
        public PartialViewResult AdminDashboard()
        {
            var model = new HovDashBoard();
            return PartialView("_AdminDashboard",model);
        }
        public class BasicProjectInfo
        {
            public string UserName { get; set; }
            public Guid Id { get; set; }
        }

        [Route("Admin/UserAccountsRead")]
        public JsonResult UserAccountsRead([DataSourceRequest]DataSourceRequest request)
        {
            //List<MyOHRole> models = new List<MyOHRole>();
            //models = RoleManager.Roles.ToList();  
            //var models = new List<MyOHUser>();
            ////models = RoleManager.Roles.ToList();
            //models = UserManager.Users;

            var users = UserManager.Users.ToList().Select(u => new BasicProjectInfo { UserName = u.UserName, Id= u.Id});

            return Json(users.ToDataSourceResult(request));

        }
        
        public PartialViewResult UserAccountDashboard()
        {
            //ViewBag.StatusMessage =
            //    message == ManageMessageId.RemoveLoginSuccess ? "The external login was removed."
            //    : message == ManageMessageId.Error ? "An error has occurred."
            //    : "";
            //var user = await UserManager.FindByIdAsync(new Guid(User.Identity.GetUserId()));
            //if (user == null)
            //{
            //    return PartialView("Error");
            //}

            //UsersViewModel um = new UsersViewModel();
            //um.MyUsers = UserManager.Users.ToList();

            //var userLogins = await UserManager.GetLoginsAsync(new Guid(User.Identity.GetUserId()));
            //var otherLogins = AuthenticationManager.GetExternalAuthenticationTypes().Where(auth => userLogins.All(ul => auth.AuthenticationType != ul.LoginProvider)).ToList();
            //ViewBag.ShowRemoveButton = userLogins.Count > 1;
            //user.PasswordHash != null || 

            return PartialView("_UserAccountDashboard");
            
        }

        public PartialViewResult VolunteerDashboard()
        {

            return PartialView("_VolunteerDashboard");
        }
        public PartialViewResult WorkflowDashboard()
        {
            ViewBag.Tasks = _workflowBLLService.GetWorkflowItemViewModel();
            ViewBag.Dependencies = _workflowBLLService.GetDependencyViewModel();
            return PartialView("_WorkflowDashboard");
        }
        public PartialViewResult GroupDashboard()
        {

            return PartialView("_GroupDashboard");
        }

        //
        // GET: /Manage/ManageLogins
        public async Task<ActionResult> ManageLogins(ManageMessageId? message)
        {
            ViewBag.StatusMessage =
                message == ManageMessageId.RemoveLoginSuccess ? "The external login was removed."
                : message == ManageMessageId.Error ? "An error has occurred."
                : "";
            var user = await UserManager.FindByIdAsync(new Guid(User.Identity.GetUserId()));
            if (user == null)
            {
                return View("Error");
            }
            var userLogins = await UserManager.GetLoginsAsync(new Guid(User.Identity.GetUserId()));
            var otherLogins = AuthenticationManager.GetExternalAuthenticationTypes().Where(auth => userLogins.All(ul => auth.AuthenticationType != ul.LoginProvider)).ToList();
            ViewBag.ShowRemoveButton = userLogins.Count > 1;
            //user.PasswordHash != null || 
            return View(new ManageLoginsViewModel
            {
                CurrentLogins = userLogins,
                OtherLogins = otherLogins
            });
        }


        private IEnumerable<AdminNavigationItemViewModel> GetInlineData()
    {
        List<AdminNavigationItemViewModel> inline = new List<AdminNavigationItemViewModel>
            {
                new AdminNavigationItemViewModel
                {
                    CategoryName = "HOV Administrator",
                    Controller = "/Administrator/Admin",
                    Action = "Index",
                    SubCategories = new List<AdminNavigationSubItemViewModel>
                    {
                        new AdminNavigationSubItemViewModel()
                        {
                            SubCategoryName = "User Account Management",
                            Controller = "/Administrator/Admin",
                            Action = "UserAccountDashboard"
                        },
                        new AdminNavigationSubItemViewModel()
                        {
                            SubCategoryName = "Support Group Management",
                            Controller = "/Administrator/Admin",
                            Action = "UserAccountDashboard",
                        }
                    }
                },new AdminNavigationItemViewModel
                {
                    CategoryName = "Volunteer Management",
                        Controller = "/Administrator/Admin",
                    Action = "VolunteerDashboard",
                    SubCategories = new List<AdminNavigationSubItemViewModel>
                    {
                        new AdminNavigationSubItemViewModel()
                        {
                            SubCategoryName = "New Applicants" ,
                                Controller = "Program",
                    Action = "Details"                             
                        }
                        
                    },
                },
                new AdminNavigationItemViewModel
                {
                    CategoryName = "Workflow Management",
                    Controller = "/Administrator/Admin",
                    Action = "WorkflowDashboard",
                    SubCategories = new List<AdminNavigationSubItemViewModel>
                    {
                        new AdminNavigationSubItemViewModel()
                        {
                            SubCategoryName = "Program Performance Workflow",
                            Controller = "/Administrator/Workflow",
                            Action = "Index"
                        }
                    }
                },
                new AdminNavigationItemViewModel
                {
                    CategoryName = "Support Groups",
                        Controller = "Program",
                    Action = "Details",
                    SubCategories = new List<AdminNavigationSubItemViewModel>
                    {
                        new AdminNavigationSubItemViewModel()
                        {
                            SubCategoryName = "Dashboard" ,
                            Controller = "/HOV/SupportGroups",
                            Action = "Index"                             
                        },
                        new AdminNavigationSubItemViewModel
                        {
                            SubCategoryName = "",
                            Controller = "/Program",
                            Action = "Details"
                        }
                    },
                }
                ,
                new AdminNavigationItemViewModel
                {
                    CategoryName = "Scheduling",
                        Controller = "Program",
                    Action = "Details",
                    SubCategories = new List<AdminNavigationSubItemViewModel>
                    {
                        new AdminNavigationSubItemViewModel()
                        {
                            SubCategoryName = "Volunteer Events" ,
                            Controller = "Program",
                            Action = "Details"                             
                        },
                        new AdminNavigationSubItemViewModel
                        {
                            SubCategoryName = "",
                            Controller = "/Program",
                            Action = "Details"
                        }
                    },
                }
                ,
                new AdminNavigationItemViewModel
                {
                    CategoryName = "Reports",
                        Controller = "Program",
                    Action = "Details",
                    SubCategories = new List<AdminNavigationSubItemViewModel>
                    {
                        new AdminNavigationSubItemViewModel()
                        {
                            SubCategoryName = "Volunteer Events" ,
                            Controller = "/WidgetFactory/Category",
                            Action = "Showtree",
                            Url = "?categoryid=F55AE2C5-F70B-40B8-BC9F-95761EF4045B"
                        },
                        new AdminNavigationSubItemViewModel
                        {
                            SubCategoryName = "",
                            Controller = "/Program",
                            Action = "Details"
                        }
                    },
                }
            };

        return inline;
 
    }
        public enum ManageMessageId
        {
            AddPhoneSuccess,
            ChangePasswordSuccess,
            SetTwoFactorSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
            RemovePhoneSuccess,
            Error
        }
    }
}