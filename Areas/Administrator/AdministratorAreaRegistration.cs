﻿using System.Web.Mvc;

namespace OH.Net.Areas.Administrator
{
    public class AdministratorAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Administrator";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
              "Administrator_widget",
              "Administrator/WidgetFactory/Category/Showtree/{id}",
              new { controller = "Category", action = "Showtree", id = UrlParameter.Optional },
              namespaces: new[] { "OH.Net.Areas.WidgetFactory.Controllers" }
              );
            context.MapRoute(
                "Administrator_default",
                "Administrator/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}