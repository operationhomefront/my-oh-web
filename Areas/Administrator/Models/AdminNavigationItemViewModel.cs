﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;

namespace OH.Net.Areas.Administrator.Models
{
    public class AdminNavigationItemViewModel
    {
        public string CategoryName { get; set; }
        public RouteValueDictionary Url { get; set; }
        public string Action { get; set; }
        public string Controller { get; set; }
        public List<AdminNavigationSubItemViewModel> SubCategories { get; set; }
    }

    public class AdminNavigationSubItemViewModel
    {
        public string SubCategoryName { get; set; }
        public RouteValueDictionary Url { get; set; }
        public string Action { get; set; }
        public string Controller { get; set; }
        public List<AdminNavigationSubItemViewModel> SubCategories { get; set; }
    }
}