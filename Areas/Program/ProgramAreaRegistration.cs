﻿using System.Security.Policy;
using System.Web.Mvc;

namespace OH.Net.Areas.Program
{
    public class ProgramAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Program";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            //context.MapRoute("pop","Program/Attachment",new { controller = "Account", action = "ExternalLoginCallbackRedirect" });
            context.MapRoute("Program_LogIn", "Program/Account/{action}", new { AreaName = "Program", controller = "Profile"}, namespaces: new[] { "OH.Net.Areas.Program.Controllers" }
           );

            context.MapRoute(
            "Program_Upload",
            "Program/Category/Upload/{id}",
            new { controller = "Category", action = "Upload", id = UrlParameter.Optional },namespaces: new[] { "OH.Net.Areas.WidgetFactory.Controllers" }
            );


            context.MapRoute(
            "Program_UploadCareGiverAttachment",
            "Program/Category/UploadCareGiverAttachment/{id}",
            new { controller = "Category", action = "UploadCareGiverAttachment", id = UrlParameter.Optional },
            namespaces: new[] { "OH.Net.Areas.WidgetFactory.Controllers" }
            );

            context.MapRoute(
           "Program_RemoveCareGiverAttachment",
           "Program/Category/RemoveCareGiverAttachment/{id}",
           new { controller = "Category", action = "RemoveCareGiverAttachment", id = UrlParameter.Optional },
           namespaces: new[] { "OH.Net.Areas.WidgetFactory.Controllers" }
           );

            context.MapRoute(
             "Program_ShowTree",
             "Program/Documents/ShowTree/{id}",
             new { controller = "Category", action = "ShowTree", id = UrlParameter.Optional },
             namespaces: new[] { "OH.Net.Areas.WidgetFactory.Controllers" }
             );

            context.MapRoute(
                "Program_default",
                "Program/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}