﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OH.Net.Areas.Program.Models
{
    public class vmApplicationStatus
    {
            public string ProgramName { get; set; }
            public string Status { get; set; }
            public string SubmittedOn { get; set; }
            public Guid Id { get; set; }
     
    }
}