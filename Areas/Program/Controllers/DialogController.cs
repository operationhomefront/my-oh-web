﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OH.Net.Areas.Program.Controllers
{
    public class DialogController : Controller
    {
        // GET: Program/Dialog
        public ActionResult Index()
        {
            return View();
        }
        
        public ActionResult Details(int id)
        {
            vmRegisterToolTip model = new vmRegisterToolTip();
            model.ToolTip = ListOptions.FirstOrDefault(x=>x.Value==id);

            return PartialView(model);
        }

        public ActionResult FilteredSubList(int id)
        {
            vmRegisterToolTip model = new vmRegisterToolTip();
            model.ToolTip = ListOptions.FirstOrDefault(x => x.Value == id);

            return
                Json(
                    ListOptions.Select(x => new { InternalName = x.Title, InternalValue = x.Value, x.Parent })
                        .Where(x => x.Parent == id), JsonRequestBehavior.AllowGet);
        }


        private IEnumerable<RegisterToolTip> ListOptions
        {
            get
            {
                return new Collection<RegisterToolTip>  { 
                    new RegisterToolTip(){Value= 1, Title="Apply for emergency financial assistance.",Body="body for spouse",Parent=1 },
                    new RegisterToolTip(){Value= 3, Title="Apply for a mortgage-free home.",Body="body for spouse",Parent=1 },
                    new RegisterToolTip(){Value= 4, Title="Apply for free transitional housing.",Body="body for spouse",Parent=1 },
                    new RegisterToolTip(){Value= 2, Title="View and/or register for events in my area.",Body="body for spouse",Parent=1 },
                    new RegisterToolTip(){Value= 4, Title="View Military Child of The Year.",Body="body for spouse",Parent=1 },
                    new RegisterToolTip(){Value= 5, Title="Join Operation Homefront’s caregiver program.",Body="body for spouse",Parent=2 },
                    new RegisterToolTip(){Value= 1, Title="Apply for emergency financial assistance.",Body="body for spouse",Parent=2 },
                    
                    new RegisterToolTip(){Value= 6, Title="Sign up to pick up donations from Dollar Tree stores.",Body="Sign up to pick up donations from Dollar Tree stores.",Parent=4 }

                    
                };
            }
        }

        public class vmRegisterToolTip
        {
            public RegisterToolTip ToolTip { get; set; }
        }

        public class RegisterToolTip
        {
            public int Value { get; set; }
            public string Title { get; set; }
            public string Body { get; set; }
            public int Parent { get; set; }
            public string MoreLink { get; set; }
        }

    }
}