﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using OH.DAL.Migrations;
//using OH.Helpers.ActiveDirectory;
using OH.Net.Areas.Administrator.Controllers;

namespace OH.Net.Areas.Program.Controllers
{
    
    public class DashboardController : Controller
    {
        // GET: Program/Dashboard
        public ActionResult Index()
        {
            var model = new vmMessageModel();
            return View(model.messages);
        }

         public JsonResult Read([DataSourceRequest]DataSourceRequest request)
        {
            //List<MyOHRole> models = new List<MyOHRole>();
            //models = RoleManager.Roles.ToList();  
            //var models = new List<MyOHUser>();
            ////models = RoleManager.Roles.ToList();
            //models = UserManager.Users;

            //var users = UserManager.Users.ToList().Select(u => new AdminController.BasicProjectInfo { UserName = u.UserName, Id= u.Id});
            var model = new vmMessageModel();

            return Json(model.messages.ToDataSourceResult(request));
        }

    }
     public class vmMessageModel
        {
            public ICollection<vmMessage> messages { get; set; }

            public vmMessageModel()
            {
                List<vmMessage> cl = new List<vmMessage>();
                vmMessage vm = new vmMessage();
                vm.Id = Guid.NewGuid();
                vm.Message = @"<b>Welcome to myOperationHomefront.net!</b>
    <p>With this site, you’ll be able to manage registrations, applications and more options that we’ll be adding throughout 2015. To find out more about our programs, visit our website. Or feel free to follow us on Facebook, Twitter, Pinterest or Instagram. 

                            We are honored to provide eligible military families:
                            
                            <ul>
                                <li>RELIEF during a crisis</li>
<li>a place to RECOVER if/when they need it</li>
<li>a little bit of RECOGNITION for a life of sacrifice </li>
                            </p>
                    <p>We’re happy you’re here and we look forward to serving you!</p>
                            ";
                vm.url = "Images/OH-logo-shadow.png";
                cl.Add(vm);

                vm = new vmMessage();
                vm.Id = Guid.NewGuid();
                vm.Message = @"<b>Upcoming Event in your area</b>
    <p>                     We are honored to provide eligible military families:
                            
                            <ul>
                                <li>RELIEF during a crisis</li>
<li>a place to RECOVER if/when they need it</li>
<li>a little bit of RECOGNITION for a life of sacrifice </li>
                            </p>
                    <p>We’re happy you’re here and we look forward to serving you!</p>
                            ";
                vm.url = "Images/OH-logo-shadow.png";
                cl.Add(vm);

                vm = new vmMessage();
                vm.Id = Guid.NewGuid();
                vm.Message = @"<b>Messag 3</b><p>We are honored to provide eligible military families:</p>
                    <p>We’re happy you’re here and we look forward to serving you!</p>
                            ";
                vm.url = "Images/OH-logo-shadow.png";
                cl.Add(vm);
                messages = cl;
            }

        }

        public class vmMessage
        {
            public string Message { get; set; }
            public string url { get; set; }
            public Guid Id { get; set; }

            public vmMessage()
            {
                
            }
        }
    }

