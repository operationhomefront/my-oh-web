﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OH.Net.Areas.Program.Controllers
{
    public class ProfileController : Controller
    {
        // GET: Program/Profile
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Login()
        {
            return RedirectToAction("login", "account", new { area = "" });
        }
        public ActionResult Logoff()
        {
            return RedirectToAction("LogOffCustom", "account", new { area = "" });
        }
    }
}