﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Configuration;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
//using OH.Email;
//using OH.Helpers.Strings;
using OH.Net.Code;
using OH.Net.Code.HelperEmail;
using OH.Net.Code.viewModels;
using OH.Net.Models;

namespace OH.Net.Areas.Program.Controllers
{
    public class RegisterController : Controller
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;
        private ApplicationRoleManager _roleManager;
        public RegisterController(ApplicationUserManager userManager, ApplicationSignInManager signInManager, ApplicationRoleManager roleManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
            RoleManager = roleManager;


        }
        public ApplicationRoleManager RoleManager
        {
            get
            {
                return _roleManager ?? HttpContext.GetOwinContext().Get<ApplicationRoleManager>();
            }
            private set
            {
                _roleManager = value;
            }
        }
        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set 
            { 
                _signInManager = value; 
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        [AllowAnonymous]
        public ActionResult InitialInquiry(string returnUrl)
        {
            return RedirectPermanent("/Program/InitialInquiry");
        }



        // GET: Program/Register
        
        public async Task<ActionResult> Index(vmWorkflowAccountCreate model)
        {
            if (model.ConfirmationCode != null)
            {
                if (model.ConfirmationCode != User.Identity.GetUserId().Substring(31, 5))
                {
                    return Json(new { Success = 0, StatusMessage = "Wrong Code"});
                }

                //sum up all the values from the list
                int roleValues = 0;
                foreach (string s in model.ProfileSelected1)
                {
                    roleValues += Convert.ToInt32(s);
                }
                foreach (string s in model.ProfileSelected2)
                {
                    roleValues += Convert.ToInt32(s);
                }

                if ((roleValues & 4)/4 == 1)
                {
                    if (RoleManager.RoleExists("HOV:Inquiry"))
                        await UserManager.AddToRoleAsync(new Guid(User.Identity.GetUserId()), "HOV:Inquiry");
                }

                
                if ((roleValues & 512)/512 == 1)
                {

                }

                string newurl = Url.Action("InitialInquiry");
                //return RedirectToAction("InitialInquiry", "Program");

                return Json(new { Success = 3, StatusMessage = newurl });
            }
            else
            {


                if (ModelState.IsValid)
                {
                    var user = new MyOHUser {UserName = model.vmRegister.Email, Email = model.vmRegister.Email};
                    var result = await UserManager.CreateAsync(user, model.vmRegister.Password);
                    if (result.Succeeded)
                    {
                        //now we can add to a role 


                        await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);

                        // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                        // Send an email with this link
                        //string code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
                        //var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                        //await UserManager.SendEmailAsync(user.Id, "Confirm your account", "Please confirm your account by clicking <a href=\"blank\">here</a>");

                        /************************* email client *************************/
                        //var email = new EmailJob("")
                        //{
                        //    Subject = "Confirm your account",
                        //    From = "roy.serna@operationhomefront.net",
                        //    To = model.vmRegister.Email,
                        //    Body = String.Format(@"Your confirmation code is {0}", user.Id.ToString().Substring(31, 5))
                        //};
                        //email.Send();
                        /************************* email client *************************/

                        model.ErrorCount = -1;
                        model.ErrorMessage = "ConfirmEmailSent";

                        //var res = Json(new {Success = true, StatusMessage = "ConfirmEmailSent"});

                        return Json(new {Success = 1, StatusMessage = "ConfirmEmailSent"});
                        //return RedirectToAction("Index", "Home");
                    }

                    model.ErrorCount = 1;
                    model.ErrorMessage = AddErrors(result);
                    return Json(new {Success = 0, StatusMessage = model.ErrorMessage});
                    //AddErrors(result);
                }

                // If we got this far, something failed, redisplay form
                return Json(new {Success = 0, StatusMessage = "Modelstate error"});
                //return View(model);
            }
        }

        private string AddErrors(IdentityResult result)
        {
            string rtn = "";
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
                rtn += error;
            }
            return rtn;
        }
    }
}