﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using OH.BLL.Services.Attachments;
using OH.BLL.Services.Categories;

namespace OH.Net.Areas.Program.Controllers
{
    public class DocumentsController : Controller
    {
        private ApplicationUserManager _userManager;
        private ApplicationRoleManager _roleManager;
        private readonly ICategoryBllService _categoryBllService;
        private readonly IAttachmentBllService _attachmentBllService;


        public DocumentsController(ApplicationUserManager userManager, ApplicationSignInManager signInManager, ApplicationRoleManager roleManager, ICategoryBllService categoryBllService, IAttachmentBllService attachmentBllService)
        {
            UserManager = userManager;
            RoleManager = roleManager;
            _categoryBllService = categoryBllService;
            _attachmentBllService = attachmentBllService;
        }

        public ApplicationRoleManager RoleManager
        {
            get
            {
                return _roleManager ?? HttpContext.GetOwinContext().Get<ApplicationRoleManager>();
            }
            private set
            {
                _roleManager = value;
            }
        }
        
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        // GET: Program/Documents
        public ActionResult Index()
        {
            var ownerId = new Guid(System.Web.HttpContext.Current.User.Identity.GetUserId());

            var model = _categoryBllService.GetCategoryTreeViewModelByOwnerId(ownerId);

            return View(model);
        }
    }
}