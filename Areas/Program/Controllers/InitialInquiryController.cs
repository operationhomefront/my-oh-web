﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Net.Configuration;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using OH.BLL.Models.Addresseses;
using OH.BLL.Models.Attachments;
using OH.BLL.Models.Persons;
using OH.BLL.Models.ProgramProfiles.Caregiver;
using OH.BLL.Models.ProgramProfiles.Military;
using OH.BLL.Services.ListTypes.Military;
using OH.BLL.Services.ProgramProfiles.Caregiver;
using OH.BLL.Services.ProgramProfiles.Military;
//using OH.Helpers.ActiveDirectory;
using OH.Net.Code;
using OH.Net.Code.viewModels;
using OH.BLL.Services.ProgramProfiles;

namespace OH.Net.Areas.Program.Controllers
{
    
    public class InitialInquiryController : Controller
    {
        // GET: Program/InitialInquiry

        private ApplicationUserManager _userManager;
        private ApplicationRoleManager _roleManager;
        
        public IProgramProfileCaregiverBLLService _programCaregiverService { get; set; }
        public IProgramProfileMilitaryBLLService _programMilitaryService { get; set; }
        public IProgramProfileWorkflowBLLService _programProfileWorkflowBLLService { get; set; }

        public InitialInquiryController(ApplicationUserManager userManager, ApplicationSignInManager signInManager, ApplicationRoleManager roleManager, 
            IProgramProfileMilitaryBLLService programMilitaryService, IProgramProfileCaregiverBLLService programCaregiverService,
            IProgramProfileWorkflowBLLService programProfileWorkflowBLLService)
        {
            UserManager = userManager;
            RoleManager = roleManager;
            _programMilitaryService = programMilitaryService;
            _programCaregiverService = programCaregiverService;
            _programProfileWorkflowBLLService = programProfileWorkflowBLLService;
        }

        public ApplicationRoleManager RoleManager
        {
            get
            {
                return _roleManager ?? HttpContext.GetOwinContext().Get<ApplicationRoleManager>();
            }
            private set
            {
                _roleManager = value;
            }
        }
        
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public ActionResult Index()
        {
            //determine the default state of the inquiry from the roles table
            
            var roles = UserManager.GetRoles(new Guid(System.Web.HttpContext.Current.User.Identity.GetUserId()) );

            if (roles.Contains("HOV:Inquiry"))
            {
                //load the HOV portion of the inquiry

            }
            vmInquiryWorkFlowBase model = new vmInquiryWorkFlowBase();
            //var m = new PersonViewModel() { FirstName = "Grace" };
            //model.Submitter.PersonCaregiver = m;
            return View(model);
        }

        public PartialViewResult InitialCareGiver(vmInquiryWorkFlowBase model1 )
        {
            vmInquiryWorkFlowBase model = new vmInquiryWorkFlowBase();
            var t = model.Displays.FirstOrDefault(x => x.StepNumber == 1);
            t.Enabled = true;

            ViewData["Selectors"] = _programProfileWorkflowBLLService.GetCaregiverInitialInquiryWorkflow();

            model.Submitter = _programCaregiverService.Initialize(new Guid(System.Web.HttpContext.Current.User.Identity.GetUserId()),System.Web.HttpContext.Current.User.Identity.Name);
            
            return PartialView("_InitialCareGiver", model.Submitter);
        }

        [HttpPost]
        public JsonResult SaveInitialCareGiver(ProfileCaregiverViewModel model1)
        {
            //return Json(new {new ProfileCaregiverViewModel()}, JsonRequestBehavior.AllowGet);
            vmInquiryWorkFlowBase model = new vmInquiryWorkFlowBase();
            var t = model.Displays.FirstOrDefault(x => x.StepNumber == 2);
            t.CategoryId = new Guid("6C280255-F50C-490A-99F8-FB1C194F8422");
            t.Enabled = true;
            
            string message = string.Format("Successfully processed.");
            
            if (_programCaregiverService.Save(model1))
            {
                Response.StatusCode = (int)HttpStatusCode.OK;
                return Json(new { Success = true, Message = message }, JsonRequestBehavior.AllowGet);//return PartialView("_ServiceMember", model1.ServiceMemberProfile);
            }
            else
            {
                message = "Error saving the record";
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(new { Success = false, Message = message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult SaveInitialServiceMember(ProfileMilitaryViewModel model1)
        {
            //return Json(new { Success = true, Message = "Test" }, JsonRequestBehavior.AllowGet);
            string message = string.Format("Successfully processed.");
            
            if (_programMilitaryService.Save(model1))
            {
                Response.StatusCode = (int)HttpStatusCode.OK;
                return Json(new { Success = true, Message = message }, JsonRequestBehavior.AllowGet);//return PartialView("_ServiceMember", model1.ServiceMemberProfile);
            }
            else
            {
                message = "Error saving the record";
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(new { Success = false, Message = message }, JsonRequestBehavior.AllowGet);
            }
        }
        public PartialViewResult ServiceMember(vmInquiryWorkFlowBase model1)
        {
            vmInquiryWorkFlowBase model = new vmInquiryWorkFlowBase();
            var t = model.Displays.FirstOrDefault(x => x.StepNumber == 2);
            var m = new PersonViewModel() { FirstName = "Grace" };
            m.Id = new Guid(System.Web.HttpContext.Current.User.Identity.GetUserId());
            t.CategoryId = new Guid("6C280255-F50C-490A-99F8-FB1C194F8422");
            t.Enabled = true;

            model.MilitaryProfile = _programMilitaryService.Initialize(m.Id); //new Guid(System.Web.HttpContext.Current.User.Identity.GetUserId()), System.Web.HttpContext.Current.User.Identity.Name);
           
            return PartialView("_ServiceMember", model.MilitaryProfile);
        }
        public PartialViewResult Summary(vmInquiryWorkFlowBase model1)
        {
            vmInquiryWorkFlowBase model = new vmInquiryWorkFlowBase();
            var t = model.Displays.FirstOrDefault(x => x.StepNumber == 3);
            var m = new PersonViewModel() { FirstName = "Grace" };
            m.Id = new Guid(System.Web.HttpContext.Current.User.Identity.GetUserId());
            
            t.Enabled = true;

            return PartialView("_Summary", model);
        }

       
    }
}