﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Management;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using OH.BLL;
using OH.BLL.Models.Events;
using OH.BLL.Models.TimeStamps;
using OH.BLL.Services.Events;
using OH.BLL.Services.Staff;

namespace OH.Net.Areas.Events.Controllers
{
    public class CollectionEventController : Controller
    {
        private readonly ICollectionEventBllService _collectionEventBllService;
        private readonly ICollectionLocationBllService _collectionLocationBllService;
        private readonly ICollectionEventOfficeMappingBllService _collectionEventOfficeMappingBllService;
        public CollectionEventController(ICollectionEventBllService collectionEventBllService, ICollectionLocationBllService collectionLocationBllService, ICollectionEventOfficeMappingBllService collectionEventOfficeMappingBllService)
        {
            _collectionEventBllService = collectionEventBllService;
            _collectionLocationBllService = collectionLocationBllService;
            _collectionEventOfficeMappingBllService = collectionEventOfficeMappingBllService;
        }

        // GET: Events/CollectionEvent
        public ActionResult Index()
        {
            ViewBag.inline = (new StaffMenuService()).GetInlineData();
            return View();
        }



        [HttpGet]
        public PartialViewResult New()
        {
            ViewBag.Offices = _collectionEventBllService.GetOffices();
            ViewBag.isNew = true;

            var g = _collectionEventBllService.Initialize();
            return PartialView("_New", g);
        }
        //[HttpGet]
        //public JsonResult GetEmpty()
        //{
        //    ViewBag.Offices = _collectionEventBllService.GetOffices();
        //    return Json(data: new CollectionEventViewModel(), behavior: JsonRequestBehavior.AllowGet);
        //}

        [HttpPost]
        public PartialViewResult New(CollectionEventViewModel c)
        {

            //Tuck Stuff
            //if (!ModelState.IsValid)
            //{
            // throw new Exception("Unexpected model state errror when saving new collection event");   
            //}
            ////do save
            //var g=_collectionEventBllService.Initialize();
            ////c = _collectionEventBllService.GetById(g);
            //return PartialView("_CollectionEventContainer",g);


            //c.Participants = _collectionEventBllService.GetOffices(c.Participants);



            if (ModelState.IsValid)
            {
                var g = _collectionEventBllService.UpdateEvent(c);
                return PartialView("_CollectionEventContainer", g);
            }
            ViewBag.isNew = false;
            ViewBag.Offices = _collectionEventBllService.GetOffices();
            return PartialView("_New", c);

        }

        public JsonResult GetOfficesList()
        {
            var offices = _collectionEventBllService.GetOffices();
            return Json(offices, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public PartialViewResult Edit(string id)
        {
            ViewBag.Offices = _collectionEventBllService.GetOffices();
            ViewBag.isNew = false;
            var c =  _collectionEventBllService.GetById(new Guid(id));
            
            return PartialView("_New", c);
        }

        [HttpPost]
        public void Edit(CollectionEventViewModel c)
        {
            var g = _collectionEventBllService.UpdateEvent(c);
            //return RedirectToAction("GetEventTabs", new {id = g.ToString()});
            //return PartialView("_CollectionEventContainer", new Guid(c.MeetingID));
        }
        //public ActionResult Locations(Guid id)
        //{
            
        //    return PartialView("_Locations",id);
        //}

        public ActionResult Locations(string id)
        {
            return PartialView("_Locations", id);
        }

       // public PartialViewResult GetEventTabs(string id) { return PartialView("_CollectionEventContainer", new Guid(id)); }
        public PartialViewResult GetEventTabs(Guid id) { return PartialView("_CollectionEventContainer", id); }
        [HttpPost]
        public JsonResult Create_CollectionLocation([DataSourceRequest] DataSourceRequest request, CollectionLocationMappingViewModel collectionLocationMappingViewModel)
        {
            return Json(new[] {_collectionEventBllService.AddCollectionLocationMapping(collectionLocationMappingViewModel)}.ToDataSourceResult(request));
        }


        public JsonResult Read_CollectionLocation([DataSourceRequest] DataSourceRequest request, string id)
        {
            

            var c = _collectionEventBllService.GetById(new Guid(id));
            var m =  c.Locations ?? _collectionLocationBllService.Initialize(); //_collectionEventBllService.GetOffices(c.Participants.Select(p => p.CollectionOfficeId).ToArray());
            return Json(m.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Update_CollectionLocation([DataSourceRequest] DataSourceRequest request, CollectionLocationMappingViewModel collectionLocationMappingViewModel)
        {
            if (collectionLocationMappingViewModel != null && ModelState.IsValid)
                collectionLocationMappingViewModel = _collectionEventBllService.UpdateCollectionLocationMapping(collectionLocationMappingViewModel);
            else{
                foreach (var modelState in ViewData.ModelState.Values)
                {
                    foreach (var error in modelState.Errors)
                    {
                        var m = error.ErrorMessage;
                        var e = error.Exception;
                    }
                }
            }
            return Json(new[] { collectionLocationMappingViewModel }.ToDataSourceResult(request, ModelState));
        }

        [HttpPost]
        public ActionResult Delete_CollectionLocation([DataSourceRequest] DataSourceRequest request, CollectionLocationMappingViewModel collectionLocationMappingViewModel)
        {
            if (collectionLocationMappingViewModel != null)
                //TUCKER   _collectionEventBllService.DeleteCollectionLocationMapping(collectionLocationMappingViewModel.Id.Value);
                _collectionEventBllService.DeleteCollectionLocationMapping(collectionLocationMappingViewModel.Id);
            return Json(new[] { collectionLocationMappingViewModel }.ToDataSourceResult(request, ModelState));
        }

        public PartialViewResult Volunteers(Guid id)
        {
            return PartialView("_Volunteers",id);
        }

        public PartialViewResult Volunteers(string id)
        {
            return PartialView("_Volunteers", id);
        }
        public JsonResult Read_CollectionVolunteer([DataSourceRequest] DataSourceRequest request, string id)
        {
            var c = _collectionEventBllService.GetById(new Guid(id));
            var m = c.Volunteers ?? new List<CollectionVolunteerMappingViewModel>();
            return Json(m.ToDataSourceResult(request));
        }

        [HttpPost]
        public JsonResult Create_CollectionVolunteer([DataSourceRequest] DataSourceRequest request, CollectionVolunteerMappingViewModel collectionVolunteerMappingViewModel)
        {
            //TUCKER   return Json(new[] {_collectionEventBllService.AddOrUpdateCollectionVolunteerMapping(collectionVolunteerMappingViewModel)}.ToDataSourceResult(request));
            return Json(new[] {_collectionEventBllService.AddOrUpdateCollectionVolunteerMapping(collectionVolunteerMappingViewModel)}.ToDataSourceResult(request));
        }

        [HttpPost]
        public ActionResult Update_CollectionVolunteer([DataSourceRequest] DataSourceRequest request, CollectionVolunteerMappingViewModel collectionVolunteerMappingViewModel)
        {
            if (collectionVolunteerMappingViewModel != null && ModelState.IsValid)
                //TUCKER   collectionVolunteerMappingViewModel = _collectionEventBllService.AddOrUpdateCollectionVolunteerMapping(collectionVolunteerMappingViewModel);
                collectionVolunteerMappingViewModel = _collectionEventBllService.AddOrUpdateCollectionVolunteerMapping(collectionVolunteerMappingViewModel);
            return Json(new[] { collectionVolunteerMappingViewModel }.ToDataSourceResult(request, ModelState));
        }
        [HttpPost]
        public ActionResult Delete_CollectionVolunteer([DataSourceRequest] DataSourceRequest request, CollectionVolunteerMappingViewModel collectionVolunteerMappingViewModel)
        {
            if (collectionVolunteerMappingViewModel != null)
                //TUCKER   _collectionEventBllService.DeleteCollectionLocationMapping(collectionVolunteerMappingViewModel.Id.Value);
                _collectionEventBllService.DeleteCollectionLocationMapping(collectionVolunteerMappingViewModel.Id);
            return Json(new[] { collectionVolunteerMappingViewModel }.ToDataSourceResult(request, ModelState));
        }



    }
}