﻿using System.Web.Mvc;
using OH.BLL.Services.Addresses;
using OH.BLL.Services.Events;
using OH.BLL.Services.Staff;

namespace OH.Net.Areas.Events.Controllers
{
    public class StaffController : Controller
    {
        private readonly IAddressBllService _addressBllService;
        private readonly ICollectionEventBllService _collectionEventBllService;
        private readonly IDistributionEventBllService _distributionEventBllService;

        public StaffController(ICollectionEventBllService collectionEventBllService, IDistributionEventBllService distributionEventBllService,IAddressBllService addressBllService)
        {
            _collectionEventBllService = collectionEventBllService;
            _distributionEventBllService = distributionEventBllService;
            _addressBllService = addressBllService;
        }

        /// <summary>
        /// For Hilario to finish
        /// </summary>
        /// <returns></returns>
        public ActionResult Login()
        {
            return View();
        }

        /// <summary>
        /// For Hilario to finish
        /// </summary>
        /// <returns></returns>
        public ActionResult Logout()
        {
            return View();
        }

        /// <summary>
        /// For Hilario to finish
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Authenticate()
        {
            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            ViewBag.inline = (new StaffMenuService()).GetInlineData();
            //var c=_collectionEventBllService.GetById(new Guid("2662A6D6-9986-4239-9D3C-03C1AB243157"));
            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public PartialViewResult GetActiveCollectionEvents()
        {
            return PartialView("_CollectionEvent",_collectionEventBllService.GetAllActiveCollectionEvents());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public PartialViewResult GetClosedCollectionEvents()
        {
            return PartialView("_CollectionEvent", _collectionEventBllService.GetClosedCollectionEvents());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public PartialViewResult GetUpcomingCollectionEvents()
        {
            return PartialView("_CollectionEvent", _collectionEventBllService.GetUpcomingCollectionEvents());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public PartialViewResult GetActiveDistributionEvents()
        {
            return PartialView("_DistributionEvent",_distributionEventBllService.GetActiveDistributionEvents());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public PartialViewResult GetClosingActiveDistributionEvents()
        {
            return PartialView("_DistributionEvent", _distributionEventBllService.GetClosingActiveDistributionEvents());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public PartialViewResult GetClosedDistributionEvents()
        {
            return PartialView("_DistributionEvent", _distributionEventBllService.GetClosedDistributionEvents());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public PartialViewResult GetUpcomingDistributionEvents()
        {
            return PartialView("_DistributionEvent", _distributionEventBllService.GetUpcomingDistributionEvents());
        }
        
        public JsonResult VerifyCityStateZip(string zip)
        {
            return Json(_addressBllService.VerifyCityStateZip(zip), JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ViewResult MilitaryFamilies()
        {
            return null;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ViewResult ServiceMemberLookup()
        {
            return null;
        }


    }
}