﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OH.BLL.Models;
using OH.BLL.Models.Attachments;
using OH.BLL.Models.Categories;
using OH.BLL.Services.Attachments;
using OH.BLL.Services.Categories;

namespace OH.Net.Areas.WidgetFactory.Controllers
{
    public class CategoryController : Controller
    {
        private readonly ICategoryBllService _categoryBllService;
        private readonly IAttachmentBllService _attachmentBllService;

        public CategoryController(ICategoryBllService categoryBllService, IAttachmentBllService attachmentBllService)
        {
            _categoryBllService = categoryBllService;
            _attachmentBllService = attachmentBllService;
        }

        public ActionResult Create()
        {
            var model = _categoryBllService.GetNewCategoryViewModel();

            return View(model);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CategoryViewModel model)
            {
            if (!ModelState.IsValid) return View();
            var categoryId = _categoryBllService.CreateCategory(model);

            return RedirectToAction("ShowTree", new { categoryId });
        }

        public ActionResult ShowTree(string categoryId)
        {
            var viewModel = _categoryBllService.GetCategoryTreeViewModelById(new Guid(categoryId));
            return View(viewModel);
        }
        //public ActionResult ShowTree(string categoryId)
        //{
        //    CategoryTreeViewModel viewModel = _categoryBllService.InitializeTreeView(new Guid(categoryId));


        //    return View(viewModel);
        //}


        //[HttpPost]
        //public ActionResult Upload(string categoryIds, IEnumerable<HttpPostedFileBase> files)
        //{


        //    var model = new UploadAttachmentViewModel { CategoryIds = categoryIds.Split(',').ToList(), Files = files };
        //    _attachmentBllService.UploadAttachment(model);

      //      var viewModel = _categoryBllService.InitializeTreeView(categoryId);

        //    // Return an empty string to signify success
        //    return Content("");
        //}
        //[HttpPost]
        //public ActionResult UploadCareGiverAttachment()
        //{

        //    return View();
        //}

        [HttpPost]
        public ActionResult UploadCareGiverAttachment(Guid ownerId, Guid categoryId, IEnumerable<HttpPostedFileBase> files)
                {
            var model = new UploadAttachmentViewModel{CategoryId = categoryId, OwnerId = ownerId, Files = files};
            _attachmentBllService.UploadAttachment(model);

            // Return an empty string to signify success
            return Content("");
        }

        [HttpPost]
        public ActionResult RemoveCareGiverAttachment(Guid ownerId, Guid categoryId, string[] fileNames)
        {
            var s = ownerId;
            var s1 = categoryId;
            var s3 = fileNames;

            _attachmentBllService.DeleteAttachment(ownerId,fileNames[0],categoryId);

            //var model = new UploadAttachmentViewModel { CategoryId = categoryId, OwnerId = ownerId, Files = fileNames };
            //_attachmentBllService.UploadAttachment(model);

            // Return an empty string to signify success
            return Content("");
        }
        //    return Json( viewModel.Categories.Select(c=> new  { id = c.Id,  c.Name, hasChildren = c.SubCategories.Any() }).ToList(), JsonRequestBehavior.AllowGet);
        //}
    }
}

//TODO: Delete
//var flattenList = viewModel.Categories.Select(model => new[]
//{
//    model.Name
//}
//    //.Concat(model.SubCategories.Select(subCategory => subCategory.Name))
//    .Concat(model.Attachments.Select(attachment => attachment.FileName))).ToList();

//foreach (Category model in viewModel.Categories)
//{
//    foreach (var result in model.SubCategories.Select(subCategory => new[]
//    {
//        subCategory.Name
//    }
//        //.Concat(model.SubCategories.Select(subCategory => subCategory.Name))
//        .Concat(subCategory.Attachments.Select(attachment => attachment.FileName))))
//    {
//        flattenList.Add(result);
//    }
//}

//var flattenList = viewModel.Categories.Select(
//    c =>
//        new
//        {
//            c.Id,
//            c.Name,
//            SubCategories =
//                c.SubCategories.Select(cc => new { cc.Id, cc.Name, cc.SubCategories })
//                    .Union<object>(
//                        c.Attachments.Select(
//                            ca =>
//                                new { ca.Id, Name = ca.FileName, SubCategories = new List<Category>() }))
//        });

//flattenList = flattenList;