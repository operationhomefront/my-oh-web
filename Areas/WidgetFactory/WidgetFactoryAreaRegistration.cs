﻿using System.Web.Mvc;

namespace OH.Net.Areas.WidgetFactory
{
    public class WidgetFactoryAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "WidgetFactory";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "WidgetFactory_default",
                "WidgetFactory/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}