﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;

namespace OH.Net.Areas.HOV.Models
{
    public class vmAssessment
    {
    }

    public class Assessment
    {
        public string Label { get; set; }
        public string Title { get; set; }
        
        public virtual Collection<AssessmentItem> Items { get; set; } 
    }

    public class AssessmentItem
    {
        public string Label { get; set; }
        public string Title { get; set; }
    }
}