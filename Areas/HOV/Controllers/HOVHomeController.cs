﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OH.Net.Areas.HOV.Controllers
{
    public class HOVHomeController : Controller
    {
        // GET: HOV/HOVHome
        public ActionResult Index()
        {
            return View();
        }

        // GET: HOV/HOVHome/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: HOV/HOVHome/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: HOV/HOVHome/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: HOV/HOVHome/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: HOV/HOVHome/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: HOV/HOVHome/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: HOV/HOVHome/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
