﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OH.Net.Areas.HOV.Controllers
{
    public class SupportGroupsController : Controller
    {
        // GET: HOV/SupportGroups
        public PartialViewResult Index()
        {
            return PartialView();
        }
        
        [HttpPost]
        public ActionResult GroupLocations()
        {
            return Json(SupportGroupLocations());
        }
        public static IList<Marker> SupportGroupLocations()
        {
            return new Marker[]
            {
                new Marker(32.491480,-85.031052, "Serenity Smith"),
                new Marker(33.448600,-112.073300, "Linda McCoy"),
                new Marker(33.191536,-111.621333, "Valery Lozano"),
                new Marker(37.832360,-121.187128, "Brandice James"),
                new Marker(33.163050,-117.247481, "Robin Freeman"),
                new Marker(38.833600,-104.820600, "Elysse Fleece"),
                new Marker(28.074742,-80.632606, "Ashley Dolin"),
                new Marker(27.372506,-82.191417, "Michelle Stoltz"),
                new Marker(28.544608,-81.374546, "Melynda Bosch"),
                new Marker(28.544608,-82.449698, "Tara James"),
                new Marker(32.579021,-83.628144, "Sabrina Potter"),
                new Marker(41.396923,-87.324818, "Jasmine Lindley"),
                new Marker(41.619622,-86.138166, "Anna Gillam"),
                new Marker(37.959306,-97.904047, "Anna Anzo"),
                new Marker(39.200703,-97.073024, "Miriam Hahn"),
                new Marker(38.993390,-76.546985, "Leonora Garvey"),
                new Marker(43.012500,-83.687800, "Shannon Rynca"),
                new Marker(43.368032,-86.149466, "Rena Moore"),
                new Marker(35.946905,-78.794098, "Camille Leonard"),
                new Marker(35.066255,-78.889340, "Yvette Peterson"),
                new Marker(34.925262,	-78.916161, "Devon Walsh"),
                new Marker(36.111646,	-115.211941, "Heidi Woodring"),
                new Marker(43.097710,	-79.035937, "Kristin Daniels"),
                new Marker(39.832910,	-82.799688, "Brena Fearnow"),
                new Marker(39.161600,	-84.456900, "Mallory Boreland"),
                new Marker(41.171481,	-82.212594, "Dawnette Walters"),
                new Marker(45.480312,	-122.645115, "Eliza Knight"),
                new Marker(40.204788,	-78.628555, "Wanda Ickes"),
                new Marker(40.928600,	-75.407212, "Andrea Hodgins"),
                new Marker(40.413790,	-79.571782, "Kelly Jackson"),
                new Marker(40.038801,	-76.427281, "Victoria Stasiak"),
                new Marker(18.138009,	-65.818996, "Edna Dumas"),
                new Marker(33.988267,	-81.283744, "Megan Cain"),
                new Marker(36.529400,	-87.359400, "Amy Miracle"),
                new Marker(34.965326,	-101.356884, "Jessica Montgomery"),
                new Marker(32.725400,	-97.320800, "Sarita Pettus-Wakefield"),
                new Marker(33.678054,	-101.751108, "Ashton Ramsey"),
                new Marker(33.678054,	-101.751108, "Lori Sims"),
                new Marker(30.209785,	-95.662342, "Jennifer Kahn"),
                new Marker(29.472779,	-98.535643, "Anne Williams"),
                new Marker(30.578886,	-97.385708, "Tara Plybon"),
                new Marker(33.321494,	-96.055038, "Audrey Vansteenberg"),
                new Marker(40.756587,	-111.901214, "Tiffany Wick"),
                new Marker(37.105200,	-113.576700, "Chante Chidester"),
                new Marker(37.255909,	-81.366198, "Tina Wimmer"),
                new Marker(37.274605,	-77.225702, "Tiffany Williams"),
                new Marker(39.048236,	-78.238658, "Ashley Moslak"),
                new Marker(47.026450,	-122.796894, "Michelle Yi"),
                new Marker(46.871348,	-122.483245, "Sarah Jenkins"),
                new Marker(43.795456,	-91.153796, "Brittiny Knoll"),
                new Marker(28.014540,-117.378900, "Nicole Sparman")
            };
        }
        public class Marker
        {
            public Marker(double latitude, double longitude, string name)
            {
                LatLng = new double[] { latitude, longitude };
                Name = name;
            }

            public double[] LatLng { get; set; }
            public string Name { get; set; }
        }
    }
}