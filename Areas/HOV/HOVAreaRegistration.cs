﻿using System.Web.Mvc;

namespace OH.Net.Areas.HOV
{
    public class HOVAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "HOV";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "HOV_default",
                "HOV/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}