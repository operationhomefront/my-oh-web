﻿using OH.BLL.Models.Attachments;
using OH.Net.Models;
using OH.BLL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace OH.Net.Controllers.Api
{
    public class AttachmentController : ApiController
    {

        // GET: api/Attachments/5
        public AttachmentViewModel Get()
        {
            var model = new AttachmentViewModel();
            

            return model;
        }

        // POST: api/Attachments
        //public void Post(AttachmentViewModel attachmentViewModel)
        //{
        //    var model = attachmentViewModel;
        //    model = attachmentViewModel;
        //}

        [HttpPost]
        // POST: api/Attachments
        public async Task<HttpResponseMessage> Post()
        {

            // Check if the request contains multipart/form-data.
            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            string root = HttpContext.Current.Server.MapPath("~/App_Data");
            var provider = new MultipartFormDataStreamProvider(root);

            try
            {
                // Read the form data.
                await Request.Content.ReadAsMultipartAsync(provider);

                //// This illustrates how to get the file names.
                //foreach (MultipartFileData file in provider.FileData)
                //{
                //    Trace.WriteLine(file.Headers.ContentDisposition.FileName);
                //    Trace.WriteLine("Server file path: " + file.LocalFileName);
                //}
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (System.Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }


            //var model = attachmentViewModel;
            //model = attachmentViewModel;
            //return Request.CreateResponse(HttpStatusCode.OK);
        }

        // PUT: api/Attachments/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Attachments/5
        public void Delete(int id)
        {
        }
    }
}
