﻿using OH.BLL.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.Mvc;
using OH.BLL.Models.Attachments;
using OH.BLL.Services.Attachments;

namespace OH.Net.Controllers
{
    public class AttachmentController : Controller
    {

        private readonly IAttachmentBllService _attachmentBllService;

        public AttachmentController(IAttachmentBllService attachmentBllService)
        {
            _attachmentBllService = attachmentBllService;
        }

        public ActionResult Index()
        {
            var model = new AttachmentViewModel();

            return View(model);
        }

        public ActionResult Save(Guid categoryId, IEnumerable<HttpPostedFileBase> files)
        {  
            
            
            //var model = new AttachmentViewModel {SelectedCategoryId = categoryId, Files = files};
            //_attachmentBllService.UploadAttachment(model);


            // Return an empty string to signify success
            return Content("");
        }

        public ActionResult Remove(string[] fileNames)
        {
            // The parameter of the Remove action must be called "fileNames"

            if (fileNames != null)
            {
                foreach (var fullName in fileNames)
                {
                    var fileName = Path.GetFileName(fullName);
                    if (fileName == null) continue;
                    var physicalPath = Path.Combine(Server.MapPath("~/App_Data"), fileName);

                    // TODO: Verify user permissions

                    if (System.IO.File.Exists(physicalPath))
                    {
                        // The files are not actually removed in this demo
                        // System.IO.File.Delete(physicalPath);
                    }
                }
            }

            // Return an empty string to signify success
            return Content("");
        }


        //// GET: Angular
        //public ActionResult Index()
        //{
        //    var model = new AttachmentViewModel();

        //    return View(model);
        //}

  
        ////public AttachmentViewModel Get()
        ////{
        ////    var model = new AttachmentViewModel();

        ////    return model;
        ////}

        ////public ActionResult Submit(AttachmentViewModel model)
        ////{

        ////    if (model.Files != null)
        ////    {
        ////        TempData["UploadedFiles"] = GetFileInfo(model.Files);


        ////    }

        ////    return RedirectToAction("Result");
        ////}

        ////public ActionResult Submit(AttachmentViewModel attachmentViewModel)
        ////{
        ////    //if (files != null)
        ////    //{
        ////    //    TempData["UploadedFiles"] = GetFileInfo(files);


        ////    //}

        ////    return RedirectToAction("Result");
        ////}

        ////public ActionResult Result()
        ////{
        ////    return View();
        ////}

        //private IEnumerable<string> GetFileInfo(IEnumerable<HttpPostedFileBase> files)
        //{
        //    return
        //        from a in files
        //        where a != null
        //        select string.Format("{0} ({1} bytes)", Path.GetFileName(a.FileName), a.ContentLength);
        //}
    }
}