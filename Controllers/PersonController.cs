﻿using System.Web.Mvc;
using OH.BLL.Models.Persons;
using OH.BLL.Services.Persons;

namespace OH.Net.Controllers
{
    public class PersonController : Controller
    {
        private readonly IPersonBllService _personBllService;

        public PersonController(IPersonBllService personBllService)
        {
            _personBllService = personBllService;
        }

        // GET: Person
        public ActionResult Index()
        {
            return View();
        }

        // GET: Person/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Person/Create
        public ActionResult AddPerson()
        {
            var viewModel = new PersonViewModel();

            return View(viewModel);
        }
        [HttpPost]
        // Post: Person/Create
        public ActionResult AddPerson(PersonViewModel personViewModel)
        {
            if (ModelState.IsValid)
            {
                personViewModel.Id = _personBllService.AddPerson(personViewModel);
            }


            return View(personViewModel);
        }


        // POST: Person/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Person/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Person/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Person/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Person/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
