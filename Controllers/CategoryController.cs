﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OH.BLL.Models;
using OH.BLL.Models.Attachments;
using OH.BLL.Models.Categories;
using OH.BLL.Services.Attachments;
using OH.BLL.Services.Categories;

namespace OH.Net.Controllers
{
    public class CategoryController : Controller
    {
        private readonly ICategoryBllService _categoryBllService;
        private readonly IAttachmentBllService _attachmentBllService;

        public CategoryController(ICategoryBllService categoryBllService, IAttachmentBllService attachmentBllService)
        {
            _categoryBllService = categoryBllService;
            _attachmentBllService = attachmentBllService;
        }

        public ActionResult Create()
        {
            var model = new CategoryViewModel();

            return View(model);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CategoryViewModel model)
        {
            if (!ModelState.IsValid) return View();
            var categoryId = _categoryBllService.CreateCategory(model);

            return RedirectToAction("ShowTree", new {categoryId});
        }

        public ActionResult ShowTree(string categoryId)
        {
            CategoryTreeViewModel viewModel = _categoryBllService.InitializeTreeView(new Guid(categoryId));


            return View(viewModel);
        }

        [HttpPost]
        public ActionResult Checkboxes(CategoryTreeViewModel model)
        {
            model.SelectedCategoryIds = model.SelectedCategoryIds ?? new string[0];

            return View(model);
        }

        [HttpPost]
        public ActionResult Upload(string categoryIds, IEnumerable<HttpPostedFileBase> files)
        {


            var model = new AttachmentViewModel { CategoryIds = categoryIds.Split(',').ToList(), Files = files };
            _attachmentBllService.UploadAttachment(model);


            // Return an empty string to signify success
            return Content("");
        }



        //public JsonResult Index()
        //{
        //    var categoryId = new Guid("d0235219-7b6c-404b-8dbc-48378497daf0");

        //    var viewModel = _categoryBllService.InitializeTreeView(categoryId);


        //    return Json(new List<object>{ new
        //    {
        //        viewModel.Category.Id, viewModel.Category.Name, HasChildren = viewModel.Category.SubCategories.Any()

        //    }}, JsonRequestBehavior.AllowGet);
        //}


        private IEnumerable<CategoryTreeViewModel> GetInlineData()
        {
            CategoryTreeViewModel viewModel =
                _categoryBllService.InitializeTreeView(new Guid("d0235219-7b6c-404b-8dbc-48378497daf0"));


            return new List<CategoryTreeViewModel> {viewModel};
        }


        //public JsonResult Categories(Guid? id)
        //{


        //    var viewModel = _categoryBllService.InitializeTreeView(id.HasValue ? id.Value : id = null);


        //    return Json( viewModel.Categories.Select(c=> new  { id = c.Id,  c.Name, hasChildren = c.SubCategories.Any() }).ToList(), JsonRequestBehavior.AllowGet);
        //}
    }
}

//TODO: Delete
//var flattenList = viewModel.Categories.Select(model => new[]
//{
//    model.Name
//}
//    //.Concat(model.SubCategories.Select(subCategory => subCategory.Name))
//    .Concat(model.Attachments.Select(attachment => attachment.FileName))).ToList();

//foreach (Category model in viewModel.Categories)
//{
//    foreach (var result in model.SubCategories.Select(subCategory => new[]
//    {
//        subCategory.Name
//    }
//        //.Concat(model.SubCategories.Select(subCategory => subCategory.Name))
//        .Concat(subCategory.Attachments.Select(attachment => attachment.FileName))))
//    {
//        flattenList.Add(result);
//    }
//}

//var flattenList = viewModel.Categories.Select(
//    c =>
//        new
//        {
//            c.Id,
//            c.Name,
//            SubCategories =
//                c.SubCategories.Select(cc => new { cc.Id, cc.Name, cc.SubCategories })
//                    .Union<object>(
//                        c.Attachments.Select(
//                            ca =>
//                                new { ca.Id, Name = ca.FileName, SubCategories = new List<Category>() }))
//        });

//flattenList = flattenList;