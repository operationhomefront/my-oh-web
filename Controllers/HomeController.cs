﻿using OH.Net.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ModelState = System.Web.Http.ModelBinding.ModelState;

namespace OH.Net.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {

            return View(new OH.Net.Models.LoginViewModel());
        }
       
    }
}