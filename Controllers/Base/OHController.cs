﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
//using OH.Constants;
//using OH.Helpers.Http;
//using OH.Helpers.Strings;
//using OH.UmbracoContent.Helpers;
//using OH.UmbracoContent.Models;
//using OHv4;
//using OHv4.Models.BusinessOrg;

namespace OH.Net.Controllers.Base {

    public class OHController : Controller {

        //protected readonly Office      NationalOffice;
        protected readonly String      ClientIpAddress;
        //protected readonly OHv4Context EFctx;

        protected OHController() {
            //EFctx                    = new OHv4Context();
            //NationalOffice = EFctx.Offices.Single(x => x.InternalName == OH.Constants.Constants.NationalInternalName);
            //ClientIpAddress          = IpHelper.ClientIpAddress();
        }

        protected override void Dispose(bool disposing) {
            //EFctx.Dispose();
            //base.Dispose(disposing);
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext) {

            //ViewBag.NationalOffice  = NationalOffice;
            //ViewBag.ClientIpAddress = ClientIpAddress;
            //ViewBag.Accreditations  = SearchCms.GetList<Accreditation>("Accreditations");

            ////
            //// Set up Social Media Links
            //var tmp    = SearchCms.GetList<SocialMediaLinks>("Social Media");
            //var smList = NationalOffice.OfficeFilter(tmp);

            //if (smList.Count == 1)
            //    ViewBag.SmLinks = smList[0];
            //else
            //    ViewBag.SmLinks = null;    

            //
            // Continue with base method
            base.OnActionExecuting(filterContext);
        }
    }

    public static class BaseHelpers {
        public static List<SocialMediaLinks> OfficeFilter(this Office office, List<SocialMediaLinks> content) {
            var bc = new List<SocialMediaLinks>();
            office.FilterByOffice(content, bc);
            return bc;
        } 

        private static void FilterByOffice(this Office office, dynamic src, dynamic dst) {
            //foreach (var item in src) {
            //    if (IsEqualExtension.IsEqual(item.Office, office.InternalName)) {
            //        dst.Add(item);
            //    }
            //}
        }
    }
}