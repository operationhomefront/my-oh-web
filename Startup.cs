﻿using Microsoft.Owin;
using OH.Net;
using Owin;

[assembly: OwinStartup(typeof(Startup))]
namespace OH.Net
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
