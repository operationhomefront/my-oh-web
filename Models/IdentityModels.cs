﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.Ajax.Utilities;
using Microsoft.AspNet.Identity;
//using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity.Migrations;
using System.Configuration;
using System.Data;
using System.Threading;
using OH.DAL;
using OH.Net.Code;


namespace OH.Net.Models
{
    //// You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    //public class ApplicationUser : IdentityUser
    //{
    //    public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
    //    {
    //        // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
    //        var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
    //        // Add custom user claims here
            
    //        return userIdentity;
    //    }

    //}

   
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext()
            : base("MyOHContext")
        {
           
        }

        public virtual IDbSet<MyOHUser> MyOHUsers { get; set; }
        public virtual IDbSet<MyOHRole> MyOHRoles { get; set; }
        public virtual IDbSet<MyOHSecurityGroup> MyOHGroups { get; set; } 

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            var user = modelBuilder.Entity<MyOHUser>()
                .ToTable("MyOHUsers");
            user.Property(u => u.Id).HasColumnName("UserId");

            var passwordLogin = modelBuilder.Entity<MyOHUserMembership>()
                .HasKey(l => l.Id)
                .ToTable("MyOHUserMemberships");

            passwordLogin.HasRequired(l => l.User).WithOptional(u => u.PasswordLogin);

            user.HasMany(u => u.Roles).WithMany(r => r.Users).Map((config) =>
            {
                config
                    .ToTable("MyOHUserInRoles")
                    .MapLeftKey("UserId")
                    .MapRightKey("RoleId");
            });

            var externalLogin = modelBuilder.Entity<MyOHOAuthMembership>()
                .HasKey(l => new { l.LoginProvider, l.ProviderKey })
                .ToTable("MyOHOAuthMemberships");

            externalLogin.Property(l => l.LoginProvider).HasColumnName("Provider");
            externalLogin.Property(l => l.ProviderKey).HasColumnName("ProviderUserId");

            externalLogin.HasRequired(l => l.User).WithMany(u => u.ExternalLogins);

            var role = modelBuilder.Entity<MyOHRole>()
                .ToTable("MyOHRoles");
            role.Property(r => r.Id).HasColumnName("RoleId");
            role.Property(r => r.Name).HasColumnName("RoleName");

            role.HasRequired(r => r.Group);

            var roleGroup = modelBuilder.Entity<MyOHSecurityGroup>()
                .ToTable("MyOHSecurityGroups");
            roleGroup.Property(g => g.Id).HasColumnName("GroupId");
            roleGroup.Property(g => g.Name).HasColumnName("GroupName");


        }
    }



    


}