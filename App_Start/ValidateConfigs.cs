﻿using System;
using System.Configuration;
using System.Data.Entity;
using System.IO;
using System.Linq;
using OH.Helpers.Strings;
using OH.Logging;
using OHv4;

namespace OH.Net {

    public static class ValidateConfigs {

        //
        // Verify Entity Framework Connection
        public static void EntityFramework() {
#if DEBUG
            Database.SetInitializer<OHv4Context>(null);
#endif
            try {
                using (var ctx = new OHv4Context()) {
                    var officeCnt = ctx.Offices.Count();

                    if (officeCnt <= 0)
                        throw new ConfigurationErrorsException("No office profiles detected.");
                }
                OHLogging.LogMessage(OHLogging.LogMsgLevel.Info, "EntityFramework is valid.");
            }
            catch (Exception e) {
                var msg = e.Message;
                OHLogging.LogMessage(OHLogging.LogMsgLevel.Info, "EntityFramework exception: " + msg);
                throw new ConfigurationErrorsException("Cannot connect to EntityFramework -- " + msg);
            }
        }

        //
        // Verify MailGun Setup
        public static void MailGun() {

            if ("MailGunAPIKey".AppSettingDefined() && "MailGunBaseUrl".AppSettingDefined() && "MailGunDomain".AppSettingDefined()) {
                OHLogging.LogMessage(OHLogging.LogMsgLevel.Info, "Mailgun web.config settings validated.");
            }
            else {
                throw new Exception("ValidateWebSiteSetup - Mailgun web.config setting not pressent.");
            }
        }

        //
        // Verify Event Log Access
        public static void EventLog() {

            var siteName = "UmbracoBase".GetAppSetting();
            
            try {
                OHLogging.LogMessage(OHLogging.LogMsgLevel.Info, String.Format("Website: {0} starting.", siteName));
            }
            catch {
                throw new Exception("ValidateWebSiteSetup - Cannot write to the event log.");
            }
        }

        //
        // Verify Temp File Access
        public static void TempFileAccess(MvcApplication app) {

            const String tmpDirPath = "/tmp";
            var websiteTmpPath      = app.Server.MapPath(tmpDirPath);

            //
            // If the directory doesn't exist, create it.
            if (!Directory.Exists(websiteTmpPath)) {
                try {
// ReSharper disable once UnusedVariable
                    var dirInfo = Directory.CreateDirectory(websiteTmpPath);
                }
                catch {
                    throw new Exception(String.Format(@"ValidateWebSiteSetup - Cannot create directory ""{0}""", tmpDirPath));    
                }
            }

            //
            // Try to create a file within the directory.
            try {
                var tmpFilename = websiteTmpPath + "\\startup.tmp";
                var fs = File.Create(tmpFilename);
                fs.Close();

                try {
                    File.Delete(tmpFilename);
                }
                catch {
                    throw new Exception(String.Format(@"ValidateWebSiteSetup - Cannot delete a file from ""{0}""", tmpDirPath));
                }
            }
            catch {
                throw new Exception(String.Format(@"ValidateWebSiteSetup - Cannot create a file in ""{0}""", tmpDirPath));
            }
        }
    }
}