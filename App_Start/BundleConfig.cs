﻿using System.Web;
using System.Web.Optimization;

namespace OH.Net
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            //
            // Base JavaScript for all pages
            //
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                       "~/Scripts/jquery-2.1.3.min.js"

                        ));

            bundles.Add(new ScriptBundle("~/bundles/base").Include(
                        "~/Scripts/kendo/2015.1.408/kendo.all.min.js",
                        "~/Scripts/kendo/2015.1.408/kendo.aspnetmvc.min.js",
                        "~/Scripts/kendo/2015.1.408/kendo.timezones.min.js",
                        "~/Scripts/jquery.color-on-hover.js"
                        ));

            //
            // Base CSS for all pages
            //
            bundles.Add(new StyleBundle("~/Content/css/base").Include(
                        "~/Content/kendo/2015.1.408/kendo.common.min.css",
                        //"~/Content/kendo/2015.1.408/kendo.uniform.min.css",
                        "~/Content/kendo/2015.1.408/kendo.dataviz.min.css"
                        //"~/Content/kendo/2015.1.408/kendo.dataviz.uniform.min.css"
                        ));
            // Home Page
            bundles.Add(new StyleBundle("~/Content/css/HomePage").Include("~/Content/HomePage.css"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                //"~/Content/bootstrap.css",
                      "~/Content/site.css"));

            bundles.Add(new StyleBundle("~/Content/kendo/css").Include(
                //"~/Content/kendo/2015.1.408/kendo.common-bootstrap.min.css",
                //"~/Content/kendo/2015.1.408/kendo.bootstrap.min.css"
            "~/Content/MyOHkendoUI.css"));

            //
            // Inside Page  --  This should renamed for the appropriate page, a .css file created for the page, and "Inside.css" changed below
            //
            bundles.Add(new StyleBundle("~/Content/css/InsidePage").Include(
                      "~/Content/Site.css",
                      "~/Content/Inside.css",
                       "~/Content/MyOHkendoUI.css"
                      ));

            /******************************************************************************/


            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/kendo").Include(
            "~/Scripts/kendo/2015.1.408/kendo.all.min.js",
                 "~/Scripts/kendo/kendo.timezones.min.js", // uncomment if using the Scheduler
            "~/Scripts/kendo/2015.1.408/kendo.aspnetmvc.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/angular").Include(
            "~/Scripts/angular.js", 
            "~/Scripts/angular-resource.js"));

            bundles.Add(new ScriptBundle("~/bundles/AngularApps").Include(
            "~/Scripts/AngularApps/controllers.js"));


            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.



            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

                    }
    }
}
