using System;
using System.Data.Entity;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using Microsoft.Practices.Unity;
using OH.BLL.Services.Attachments;
using OH.BLL.Services.Categories;
using OH.BLL.Services.Households;
using OH.BLL.Services.ListTypes;
using OH.BLL.Services.ListTypes.Military;
using OH.BLL.Services.Persons;
using OH.BLL.Services.ProgramProfiles;
using OH.BLL.Services.ProgramProfiles.Caregiver;
using OH.BLL.Services.ProgramProfiles.Military;
using OH.BLL.Services.Reports;
using OH.BLL.Services.Workflow;
using OH.DAL.Domain;
using OH.DAL.Repositories;
using OH.DAL.Repositories.Addresses;
using OH.DAL.Repositories.Applications;
using OH.DAL.Repositories.Attachments;
using OH.DAL.Repositories.Authorization;
using OH.DAL.Repositories.Categories;
using OH.DAL.Repositories.ChangeLogs;
using OH.DAL.Repositories.ListTypes.Person;
using OH.DAL.Repositories.ProgramProfiles;
using OH.DAL.Repositories.ProgramProfiles.CareGiver;
using OH.DAL.Repositories.ProgramProfiles.Injury;
using OH.DAL.Repositories.ProgramProfiles.Military;
using OH.DAL.Repositories.WorkFlow;
using OH.Net.Code;
using OH.Net.Controllers;
using OH.Net.Models;
using OH.DAL.Repositories.EmailAddresses;
using OH.DAL.Repositories.Events;
using OH.DAL.Repositories.Households;
using OH.DAL.Repositories.ListTypes;
using OH.DAL.Repositories.Persons;
using OH.DAL.Repositories.Phones;
using OH.DAL.Repositories.TimeStamps;

namespace OH.Net
{
    /// <summary>
    /// Specifies the Unity configuration for the main container.
    /// </summary>
    public class UnityConfig
    {
        #region Unity Container
        private static readonly Lazy<IUnityContainer> Container = new Lazy<IUnityContainer>(() =>
        {
            var container = new UnityContainer();
            RegisterTypes(container);
            return container;
        });

        /// <summary>
        /// Gets the configured Unity container.
        /// </summary>
        public static IUnityContainer GetConfiguredContainer()
        {
            return Container.Value;
        }
        #endregion

        /// <summary>Registers the type mappings with the Unity container.</summary>
        /// <param name="container">The unity container to configure.</param>
        /// <remarks>There is no need to register concrete types such as controllers or API controllers (unless you want to 
        /// change the defaults), as Unity allows resolving a concrete type even if it was not previously registered.</remarks>
        public static void RegisterTypes(IUnityContainer container)
        {
            container.RegisterType<IPersonMilitaryBLLService, PersonMilitaryBLLService>();
            container.RegisterType<IWorkflowBLLService, WorkflowBLLService>();

            container.RegisterType<IWorkFlowItemRepository, WorkFlowItemRepository>();
            container.RegisterType<IWorkFlowItemDependencyRepository, WorkFlowItemDependencyRepository>();

            container.RegisterType<IHouseholdBLLService, HouseholdBLLService>();
            container.RegisterType<IProgramProfileWorkflowBLLService, ProgramProfileWorkflowBLLService>();

            container.RegisterType<IListTypesBLLService, ListTypesBLLService>();
            container.RegisterType<IListTypeRepository, ListTypeRepository>();

            container.RegisterType<IPersonCaregiverRepository, PersonCaregiverRepository>();

            container.RegisterType<IAttachmentRepository, AttachmentRepository>();
            container.RegisterType<IAttachmentBllService, AttachmentBllService>();

            //Applications
            container.RegisterType<IApplicationRepository, ApplicationRepository>();
            container.RegisterType<IApplicationClassRepository, ApplicationClassRepository>();

            //Address
            container.RegisterType<IAddressRepository, AddressRepository>();
            container.RegisterType<IAddressBLLService, AddressBLLService>();

            //FileType
            container.RegisterType<IFileTypeRepository, FileTypeRepository>();


            //Category
            container.RegisterType<ICategoryRepository, CategoryRepository>();
            container.RegisterType<ICategoryBllService, CategoryBllService>();
            container.RegisterType<ICategoryAttachmentRepository, CategoryAttachmentRepository>();
            //container.RegisterType<ICategoryListTypeRepository, CategoryListTypeRepository>();


            //CategoryAttachmentMapping
            container.RegisterType<ICategoryAttachmentRepository, CategoryAttachmentRepository>();

            //ChangeLog
            container.RegisterType<IChangeLogRepository, ChangeLogRepository>();

            //EmailAddressViewModel
            container.RegisterType<IEmailAddressRepository, EmailAddressRepository>();

            //Households
            container.RegisterType<IHouseholdMemberRepository, HouseholdMemberRepository>();
            container.RegisterType<IHouseholdRepository, HouseholdRepository>();

            //List Types
            container.RegisterType<IListTypeRepository, ListTypeRepository>();
            container.RegisterType<IGenderRepository, GenderRepository>();


            //Program Profiles
            container.RegisterType<IInjuryInflictionRepository, InjuryInflictionRepository>();
            container.RegisterType<IInjuryInflictionCategoryRepository, InjuryInflictionCategoryRepository>();
            container.RegisterType<IListMilitaryCombatTheaterRepository, ListMilitaryCombatTheaterRepository>();
            container.RegisterType<IProfileMilitaryRepository, ProfileMilitaryRepository>();
            container.RegisterType<IProfileMilitaryAttachmentRepository, ProfileMilitaryAttachmentRepository>();

            container.RegisterType<IListMilitaryBranchRepository, ListMilitaryBranchRepository>();
            container.RegisterType<IListMilitaryBranchBLLService, ListMilitaryBranchBllService>();

            container.RegisterType<IProgramProfileCaregiverBLLService, ProgramProfileCaregiverBLLService>();
            container.RegisterType<IProfileCaregiverRepository, ProfileCaregiverRepository>();
            container.RegisterType<IListMilitaryBranchBLLService, ListMilitaryBranchBllService>();
            container.RegisterType<ICaregiverBLLService, CaregiverBLLService>();
            container.RegisterType<IProfileCaregiverRepository, ProfileCaregiverRepository>();
            container.RegisterType<IProfileCaregiverAttachmentRepository, ProfileCaregiverAttachmentRepository>();

            container.RegisterType<IProgramProfileMilitaryBLLService, ProgramProfileMilitaryBLLService>();
            container.RegisterType<IProfileMilitaryRepository, ProfileMilitaryRepository>();



            //Person
            container.RegisterType<IPersonAddressRepository, PersonAddressRepository>();
            container.RegisterType<IPersonEmailAddressRepository, PersonEmailAddressRepository>();
            container.RegisterType<IPersonPhoneRepository, PersonPhoneRepository>();
            container.RegisterType<IPersonRepository, PersonRepository>();
            container.RegisterType<IPersonBllService, PersonBllService>();

            container.RegisterType<IPersonMilitaryRepository, PersonMilitaryRepository>();




            //Phone
            container.RegisterType<IPhoneRepository, PhoneRepository>();

            //TimeStamps
            container.RegisterType<ITimeStampRepository, TimeStampRepository>();

            container.RegisterType<UserManager<MyOHUser, Guid>>(new HierarchicalLifetimeManager());
            container.RegisterType<RoleManager<MyOHRole, Guid>>(new HierarchicalLifetimeManager());
            //container.RegisterType<IUserStore<MyOHUser, Guid>, UserStore<MyOHUser>(new HierarchicalLifetimeManager());
            container.RegisterType<DbContext, ApplicationDbContext>(new HierarchicalLifetimeManager());
            container.RegisterType<IAuthenticationManager>(new InjectionFactory(o => HttpContext.Current.GetOwinContext().Authentication));
            container.RegisterType<AccountController>(new InjectionConstructor());
            container.RegisterType<ManageController>(new InjectionConstructor());

            container.RegisterType<IReportsBllService, ReportsBllService>();
            container.RegisterType<IReportRepository, ReportRepository>();
            container.RegisterType<ICollectionEventRepository, CollectionEventRepository>();
            container.RegisterType<IDistributionEventRepository, DistributionEventRepository>();
            //container.RegisterType<IMyOhUserRepository, MyOhUserRepository>();
            //container.RegisterType<IMyOhRoleRepository, IMyOhRoleRepository>()
            container.RegisterType<IDistributionLocationRepository, DistributionLocationRepository>();
            //    ;
        }
    }
}
