﻿using System.Web.Mvc;
using System.Web.Routing;

namespace OH.Net
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
               name: "p",
               url: "Program/Attachment",
               defaults: new { controller = "Account", action = "ExternalLoginCallbackRedirect" }
           );

            //routes.MapRoute(
            //    name: "Program_Upload",
            //    url: "Program/Attachment/Upload/{id}",
            //    defaults: new { controller = "Attachment", action = "Upload", id = UrlParameter.Optional },
            //    namespaces: new [] { "OH.Net.Controllers" }
            //);

            routes.MapRoute(
               name: "Google API Sign-in",
               url: "signin-google",
               defaults: new { controller = "Account", action = "ExternalLoginCallbackRedirect" }
           );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
