namespace OH.Net.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MyOHSecurityGroups",
                c => new
                    {
                        GroupId = c.Guid(nullable: false),
                        ClusterId = c.Int(nullable: false, identity: true),
                        GroupName = c.String(),
                        ApplicationId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.GroupId);
            
            CreateTable(
                "dbo.MyOHRoles",
                c => new
                    {
                        RoleId = c.Guid(nullable: false),
                        ClusterId = c.Int(nullable: false, identity: true),
                        RoleName = c.String(),
                        Group_Id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.RoleId)
                .ForeignKey("dbo.MyOHSecurityGroups", t => t.Group_Id, cascadeDelete: true)
                .Index(t => t.Group_Id);
            
            CreateTable(
                "dbo.MyOHUsers",
                c => new
                    {
                        UserId = c.Guid(nullable: false),
                        ClusterId = c.Int(nullable: false, identity: true),
                        UserName = c.String(),
                        Email = c.String(),
                        ApplicationId = c.Guid(nullable: false),
                        MobileAlias = c.String(),
                        IsAnonymous = c.Boolean(nullable: false),
                        LastActivityDate = c.DateTime(nullable: false),
                        MobilePIN = c.String(),
                        LoweredEmail = c.String(),
                        LoweredUserName = c.String(),
                        PasswordQuestion = c.String(),
                        PasswordAnswer = c.String(),
                        IsApproved = c.Boolean(nullable: false),
                        IsLockedOut = c.Boolean(nullable: false),
                        CreateDate = c.DateTime(nullable: false),
                        LastLoginDate = c.DateTime(nullable: false),
                        LastPasswordChangedDate = c.DateTime(nullable: false),
                        LastLockoutDate = c.DateTime(nullable: false),
                        FailedPasswordAttemptCount = c.Int(nullable: false),
                        FailedPasswordAttemptWindowStart = c.DateTime(nullable: false),
                        FailedPasswordAnswerAttemptCount = c.Int(nullable: false),
                        FailedPasswordAnswerAttemptWindowStart = c.DateTime(nullable: false),
                        Comment = c.String(),
                    })
                .PrimaryKey(t => t.UserId);
            
            CreateTable(
                "dbo.MyOHOAuthMemberships",
                c => new
                    {
                        Provider = c.String(nullable: false, maxLength: 128),
                        ProviderUserId = c.String(nullable: false, maxLength: 128),
                        ClusterId = c.Int(nullable: false, identity: true),
                        Id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.Provider, t.ProviderUserId })
                .ForeignKey("dbo.MyOHUsers", t => t.Id, cascadeDelete: true)
                .Index(t => t.Id);
            
            CreateTable(
                "dbo.MyOHUserMemberships",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ClusterId = c.Int(nullable: false, identity: true),
                        CreatedDateTime = c.DateTime(nullable: false),
                        ConfirmationToken = c.String(),
                        Confirmed = c.Boolean(nullable: false),
                        LastLoginFailed = c.DateTime(),
                        LoginFailureCounter = c.Int(nullable: false),
                        PasswordHash = c.String(),
                        PasswordSalt = c.String(),
                        PasswordChanged = c.DateTime(),
                        VerificationToken = c.String(),
                        VerificationTokenExpires = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MyOHUsers", t => t.Id)
                .Index(t => t.Id);
            
            CreateTable(
                "dbo.MyOHUserInRoles",
                c => new
                    {
                        UserId = c.Guid(nullable: false),
                        RoleId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.MyOHUsers", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.MyOHRoles", t => t.RoleId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.MyOHUserInRoles", "RoleId", "dbo.MyOHRoles");
            DropForeignKey("dbo.MyOHUserInRoles", "UserId", "dbo.MyOHUsers");
            DropForeignKey("dbo.MyOHUserMemberships", "Id", "dbo.MyOHUsers");
            DropForeignKey("dbo.MyOHOAuthMemberships", "Id", "dbo.MyOHUsers");
            DropForeignKey("dbo.MyOHRoles", "Group_Id", "dbo.MyOHSecurityGroups");
            DropIndex("dbo.MyOHUserInRoles", new[] { "RoleId" });
            DropIndex("dbo.MyOHUserInRoles", new[] { "UserId" });
            DropIndex("dbo.MyOHUserMemberships", new[] { "Id" });
            DropIndex("dbo.MyOHOAuthMemberships", new[] { "Id" });
            DropIndex("dbo.MyOHRoles", new[] { "Group_Id" });
            DropTable("dbo.MyOHUserInRoles");
            DropTable("dbo.MyOHUserMemberships");
            DropTable("dbo.MyOHOAuthMemberships");
            DropTable("dbo.MyOHUsers");
            DropTable("dbo.MyOHRoles");
            DropTable("dbo.MyOHSecurityGroups");
        }
    }
}
