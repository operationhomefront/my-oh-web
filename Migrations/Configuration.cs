using OH.Net.Code;

namespace OH.Net.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<OH.Net.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            AutomaticMigrationDataLossAllowed = true;
            ContextKey = "OH.Net.Models.ApplicationDbContext";
        }

        protected override void Seed(OH.Net.Models.ApplicationDbContext context)
        {
            MyOHSecurityGroup m = MyOHSecurityGroup.GetGroup(new Guid("32BFA5EF-A476-4CB3-B838-C12297795932"),
                "Hearts Of Valor", Guid.NewGuid());

            context.MyOHGroups.AddOrUpdate(m);


            context.MyOHRoles.Add(
                new MyOHRole()
                {
                    Name = "HOV:Administrator",
                    Id = new Guid("05144597-88B3-41E4-B8EC-7AC73C047698"),
                    Group = m
                });

            context.MyOHRoles.Add(
                new MyOHRole()
                {
                    Name = "HOV:Inquiry",
                    Id = new Guid("005DDBC8-7311-4CEF-8018-FEC27D847EEC"),
                    Group = m
                });

            context.MyOHRoles.Add(
                new MyOHRole()
                {
                    Name = "HOV:Support Group Leader",
                    Id = new Guid("32DEA358-6822-41CC-BFDC-F4AB3C50A086"),
                    Group = m
                });

            context.MyOHRoles.Add(
                new MyOHRole()
                {
                    Name = "HOV:PersonCaregiver",
                    Id = new Guid("29DDD96E-F6D4-4A52-A723-70C29872108F"),
                    Group = m
                });

            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
        }
    }
}
